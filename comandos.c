#include "comandos.h"

int  AnalizarComando(char cadena[1024])
{
      int contador = 0;
      while (cadena[contador] == ' '&&cadena[contador]!='\0')
      {
          contador++;
      }

      if(cadena[contador]!='\0')
      {
          if(cadena[contador]=='#')
          {
               return -7;
          }
          else if(cadena[contador]=='L'||cadena[contador]=='l')
          {
               if(cadena[contador+1]=='E'||cadena[contador+1]=='e')
               {
                    if(cadena[contador+2]=='e'||cadena[contador+2]=='E')
                    {
                         if(cadena[contador+3]=='R'||cadena[contador+3]=='r')
                         {
                              if(cadena[contador+6]==' '||cadena[contador+6])
                              {
                                  return 9;
                              }
                         }
                    }
               }

          }
          else if(cadena[contador]=='M'||cadena[contador]=='m')
          {
          //1(mkdisk) o 4(mount)
               if(cadena[contador+1]=='K'||cadena[contador+1]=='k')
               {
                    if(cadena[contador+2]=='D'||cadena[contador+2]=='d')
                    {
                         if(cadena[contador+3]=='I'||cadena[contador+3]=='i')
                         {
                              if(cadena[contador+4]=='S'||cadena[contador+4]=='s')
                              {
                                   if(cadena[contador+5]=='K'||cadena[contador+5]=='k')
                                   {
                                        if(cadena[contador+6]==' '||cadena[contador+6])
                                        {
                                             return 1;
                                        }
                                        else return -1;
                                   }
                                   else return -1;
                              }
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;


               }
               else if(cadena[contador+1]=='O'||cadena[contador+1]=='o')
               {
                    if(cadena[contador+2]=='U'||cadena[contador+2]=='u')
                    {
                         if(cadena[contador+3]=='N'||cadena[contador+3]=='n')
                         {
                              if(cadena[contador+4]=='T'||cadena[contador+4]=='t')
                              {
                                   if(cadena[contador+5]==' '|| cadena[contador+5]=='\0')
                                   {
                                        return 4;
                                   }
                                   else return -1;
                              }
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;
            }
            else return -1;


          }


          else if(cadena[contador+0]=='R'||cadena[contador+0]=='r')
          {
               //2(rmdisk) o 6(rep)

               if(cadena[contador+1]=='M'||cadena[contador+1]=='m')
               {
                    if(cadena[contador+2]=='D'||cadena[contador+2]=='d')
                    {
                         if(cadena[contador+3]=='I'||cadena[contador+3]=='i')
                         {
                              if(cadena[contador+4]=='S'||cadena[contador+4]=='s')
                              {
                                   if(cadena[contador+5]=='K'||cadena[contador+5]=='k')
                                   {
                                        if(cadena[contador+6]==' '||cadena[contador+6]=='\0')
                                        {
                                             return 2;
                                        }
                                        else return -1;
                                   }
                                   else return -1;
                              }
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;


               }
               else if(cadena[contador+1]=='E'||cadena[contador+1]=='e')
               {
                    if(cadena[contador+2]=='P'||cadena[contador+2]=='p')
                    {
                         if(cadena[contador+3]==' '||cadena[contador+3]=='\0')
                         {
                              return 6;
                         }
                         else return -1;
                    }
                    else  if(cadena[contador+2]=='A'||cadena[contador+2]=='a')
                    {
                         if(cadena[contador+3]=='D'||cadena[contador+3]=='d')
                         {
                              if(cadena[contador+4]==' '||cadena[contador+4]=='\0')
                                   return 8;
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;


               }







          }
          else if(cadena[contador+0]=='F'||cadena[contador+0]=='f')
          {
               //3(fdisk)

               if(cadena[contador+1]=='D'||cadena[contador+1]=='d')
               {
                    if(cadena[contador+2]=='I'||cadena[contador+2]=='i')
                    {
                         if(cadena[contador+3]=='S'||cadena[contador+3]=='s')
                         {
                              if(cadena[contador+4]=='K'||cadena[contador+4]=='k')
                              {
                                   if(cadena[contador+5]==' '||cadena[contador+5]=='\0')
                                   {
                                        return 3;

                                   }
                                   else return -1;
                              }
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;


               }
               else return -1;
          }
          else if(cadena[contador+0]=='U'||cadena[contador+0]=='u')
          {
               //5(unmount)

               if(cadena[contador+1]=='N'||cadena[contador+1]=='n')
               {
                    if(cadena[contador+2]=='M'||cadena[contador+2]=='m')
                    {
                         if(cadena[contador+3]=='O'||cadena[contador+3]=='o')
                         {
                              if(cadena[contador+4]=='U'||cadena[contador+4]=='u')
                              {
                                   if(cadena[contador+5]=='N'||cadena[contador+5]=='n')
                                   {
                                        if(cadena[contador+6]=='T'||cadena[contador+6]=='t')
                                        {
                                             if(cadena[contador+6]==' '||cadena[contador+6]=='\0')
                                             {
                                                  return 5;
                                             }
                                             else return -1;

                                        }
                                        else return -1;
                                   }
                                   else return -1;
                              }
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;
               }
               else return -1;

          }
          else if(cadena[contador+0]=='E'||cadena[contador+0]=='e')
          {
               //7(exec)
               if(cadena[contador+1]=='X'||cadena[contador+1]=='x')
               {
                    if(cadena[contador+2]=='E'||cadena[contador+2]=='e')
                    {
                         if(cadena[contador+3]=='C'||cadena[contador+3]=='c')
                         {
                              if(cadena[contador+4]==' '||cadena[contador+4]=='\0')
                              {
                                   return 7;
                              }
                              else return -1;
                         }
                         else return -1;
                    }
                    else return -1;
               }
               else return -1;
          }

          else
          {
               return -1;
          }




     }
return -1;

}
//ok

Param* getParamentros(char parametros[1024])
{
     int flag=1;
     Param* params;
     params = crearNParam();
     char espacio[2]=" ";
     char guion[2]="-";
     char igual[2] = "=";
     //char separador[3] =""
     token = strtok(parametros,igual);// se divide por el igual.


     while( token != NULL )//
     {//

          //printf("TOKEN CON Q EMPIEZA EL CICLO:%s.\n",token);
          //printf("QUE ES LO QUE TIENE PARAMETROS:%s.\n",parametros);
          char* aux = token;
          char c = (*aux);
          if(c==' ')//para quitar espacios
          {
               while(c!='-'&&c==' ')
               {
                    //printf("ENTRO A QUITAR ESPACIO\n");
                    aux = aux+1;
                    c = (*aux);
               }

          }
          if(flag)
          {
               //Este corrimiento  se hace menos  cuando se pasa despues de un path!
               aux = aux+1;
               c = (*aux);


          }
          else
          {
               flag=1;
          }
          if(c=='S'||c=='s')
          {
               aux = aux+1;
               c = (*aux);
               //printf("POSIBLEMENTE ES SIZE!\n");
               if(c=='I'||c=='i')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='Z'||c=='z')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='E'||c=='e')
                         {
                              aux = aux+1;
                              c = (*aux);
                              if(c=='\0')
                              {
                                   //printf("SI ES SIZE!\n");
                                   token = strtok(NULL,espacio);// se divide hasta encontrar un espacio.
                                   //printf("Valor STRSIZE:%s.\n",token);
                                   int valsize = atoi(token);
                                   //printf("Valor size:%i.\n",valsize);
                                   params->sise = valsize;
                              }
                              else
                                   params->error=1;

                         }
                         else
                              params->error=1;

                    }
                    else
                         params->error=1;


               }
               else
                    params->error=1;

          }
          else if(c=='U'||c=='u')
          {
               aux = aux+1;
               c = (*aux);
               //printf("POSIBLEMENTE ES UNIT!\n");
               if(c=='N'||c=='n')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='I'||c=='i')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='T'||c=='t')
                         {
                              aux = aux+1;
                              c = (*aux);
                              if(c=='\0')
                              {
                                  //printf("SI ES UNIT!\n");
                                   token = strtok(NULL,espacio);
                                   char* auxx = token;
                                   char cc = (*auxx);

                                        if(cc=='k'||cc=='K')
                                        {
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                             if(cc=='\0')
                                             {
                                                  params->unit='k';
                                                  //printf("UNIT TIENE LETRA CORRECTA!\n");
                                             }
                                             else
                                             {
                                                  params->unit='e';
                                                  //printf("UNIT TIENE LETRA INCORRECTA");

                                             }


                                        }
                                        else if(cc=='m'||cc=='M')
                                        {
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                             if(cc=='\0')
                                             {
                                                 //printf("UNIT TIENE LETRA CORRECTA!\n");
                                                  params->unit='m';
                                             }
                                             else
                                             {
                                                  params->unit='e';
                                                  //printf("UNIT TIENE LETRA INCORRECTA");
                                             }

                                        }
                                        else
                                             params->unit='e';




                              }
                              else
                                   params->error=1;
                         }
                         else
                              params->error=1;
                    }
                    else
                         params->error=1;
               }
               else
                    params->error=1;


          }//fin de unit
          else if(c=='P'||c=='p')
          {
               aux = aux+1;
               c = (*aux);
               //printf("POSIBLEMENTE SEA UNPATH.\n");
               if(c=='A'||c=='a')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='T'||c=='t')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='H'||c=='h')
                         {
                              aux = aux+1;
                              c = (*aux);
                              if(c=='\0')
                              {
                                   //printf("SI ES UN PATH!\n");
                                   flag=0;
                                   token = strtok(NULL,guion);
                                   //printf("VAL DEL PATH:%s\n",token);

                                   char pathaux[150] = " ";
                                   char* auxx = token;
                                   char cc = (*auxx);
                                   if(cc=='"')
                                   {
                                        //printf("ENTRO A QUE TIENE COMILLAS\n");
                                        auxx = auxx+1;
                                        cc = (*auxx);
                                        int contador=0;

                                        while(cc!='"')
                                        {
                                             printf("%c",cc);
                                             pathaux[contador]=cc;
                                             contador++;
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                        }
                                        //printf("\nLO QUE QUEDO EN PATHAUX:%s\n",pathaux);
                                        sprintf(params->path,pathaux);

                                   }
                                   else
                                   {
                                        //printf("ENTRO A QUE NO TIENE COMILLAS\n");
                                        int contador=0;

                                        while(cc!=' ' &&cc!='\n')
                                        {
                                             //printf("%c",cc);
                                             pathaux[contador]=cc;
                                             contador++;
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                        }
                                        //printf("LO QUE QUEDO EN PATHAUX:%s\n",pathaux);
                                        sprintf(params->path,pathaux);
                                        //printf("PASO DEL SPRINTF SIN LAS COMILLAS\n");

                                   }
                              }
                              else
                                   params->error=1;
                         }
                         else
                              params->error=1;
                    }
                    else
                         params->error=1;
               }
               else
                    params->error=1;


          }//fin path
          else if(c=='T'||c=='t')
          {
               //printf("POSIBLEMENTE SEA UN TYPE");
               aux = aux+1;
               c = (*aux);
               if(c=='Y'||c=='y')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='P'||c=='p')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='E'||c=='e')
                         {
                              aux = aux+1;
                              c = (*aux);
                              if(c=='\0')
                              {
                                   //printf("SI ES TYPE!\n");
                                   token = strtok(NULL,espacio);
                                   char* auxx = token;
                                   char cc = (*auxx);

                                        if(cc=='P'||cc=='p')
                                        {
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                             if(cc=='\0')
                                             {
                                                  params->type='P';
                                                  //printf("Type TIENE LETRA CORRECTA!\n");
                                             }
                                             else
                                             {
                                                  params->type='e';
                                                  //printf("Type TIENE LETRA INCORRECTA");

                                             }


                                        }
                                        else if(cc=='e'||cc=='E')
                                        {
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                             if(cc=='\0')
                                             {
                                                  //printf("Type TIENE LETRA CORRECTA!\n");
                                                  params->type='E';
                                             }
                                             else
                                             {
                                                  params->type='e';
                                                  //printf("Type TIENE LETRA INCORRECTA");
                                             }

                                        }
                                        else if(cc=='L'||cc=='l')
                                        {
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                             if(cc=='\0')
                                             {
                                                  //printf("Type TIENE LETRA CORRECTA!\n");
                                                  params->type='L';
                                             }
                                             else
                                             {
                                                  params->type='e';
                                                  //printf("Type TIENE LETRA INCORRECTA");
                                             }

                                        }
                                        else
                                             params->type='e';



                              }
                              else
                                   params->error=1;
                         }
                         else
                              params->error=1;
                    }
                    else
                    params->error=1;
               }
               else
                    params->error=1;
          }//fin type
          else if(c=='F'||c=='f')
          {

               //printf("POSIBLEMENTE SEA UN Fit\n");
               aux = aux+1;
               c = (*aux);
               if(c=='I'||c=='i')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='T'||c=='t')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='\0')
                         {
                              //printf("SI ES FIT!\n");
                              token = strtok(NULL,espacio);
                              char* auxx = token;
                              char cc = (*auxx);
                              if(cc=='B'||cc=='b')
                              {
                                   auxx = auxx+1;
                                   cc = (*auxx);
                                   if(cc=='F'||cc=='f')
                                   {
                                        auxx = auxx+1;
                                        cc = (*auxx);
                                        if(cc=='\0')
                                        {
                                             //printf("FIT TIENE LAS LETRAS CORRECTAS!\n");
                                             sprintf(params->fit,"BF");
                                        }
                                        else
                                             sprintf(params->fit,"ee");
                                   }
                                   else
                                        sprintf(params->fit,"ee");
                              }
                              else if(cc=='F'||cc=='f')
                              {
                                   auxx = auxx+1;
                                   cc = (*auxx);
                                   if(cc=='F'||cc=='f')
                                   {
                                        auxx = auxx+1;
                                        cc = (*auxx);
                                        if(cc=='\0')
                                        {
                                             //printf("FIT TIENE LAS LETRAS CORRECTAS!\n");
                                             sprintf(params->fit,"FF");
                                        }
                                        else
                                             sprintf(params->fit,"ee");
                                   }
                                   else
                                        sprintf(params->fit,"ee");
                              }
                              else if(cc=='W'||cc=='w')
                              {
                                   auxx = auxx+1;
                                   cc = (*auxx);
                                   if(cc=='F'||cc=='f')
                                   {
                                        if(cc=='\0')
                                        {

                                             //printf("FIT TIENE LAS LETRAS CORRECTAS!\n");
                                             sprintf(params->fit,"WF");
                                        }
                                        else
                                             sprintf(params->fit,"ee");
                                   }
                                   else
                                        sprintf(params->fit,"ee");
                              }
                              else
                                   sprintf(params->fit,"ee");



                         }
                         else
                              params->error=1;

                    }
                    else
                         params->error=1;
               }
               else
                    params->error=1;
          }//fin fit
          else if(c=='D'||c=='d')
          {
               //printf("POSIBLEMENTE SEA UN DELETE\n");
               aux = aux+1;
               c = (*aux);
               if(c=='E'||c=='e')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='L'||c=='l')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='E'||c=='e')
                         {
                              aux = aux+1;
                              c = (*aux);
                              if(c=='T'||c=='t')
                              {
                                   if(c=='\0')
                                   {
                                        //printf("SI ES DELETE!\n");
                                        token = strtok(NULL,espacio);
                                        char* auxx = token;
                                        char cc = (*auxx);
                                        if(cc=='F'||cc=='f')
                                        {
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                             if(cc=='A'||cc=='a')
                                             {
                                                  auxx = auxx+1;
                                                  cc = (*auxx);
                                                  if(cc=='S'||cc=='s')
                                                  {
                                                       auxx = auxx+1;
                                                       cc = (*auxx);
                                                       if(cc=='T'||cc=='t')
                                                       {
                                                            auxx = auxx+1;
                                                            cc = (*auxx);
                                                            if(cc=='\0')
                                                            {
                                                                 //printf("DELETE TIENE LAS LETRAS CORRECTAS!\n");
                                                                 params->delet=1;
                                                            }
                                                            else
                                                                 params->delet=-7;
                                                       }
                                                       else
                                                            params->delet=-7;
                                                  }
                                                  else
                                                       params->delet=-7;
                                             }
                                             else if(cc=='U'||cc=='u')
                                             {
                                                  auxx = auxx+1;
                                                  cc = (*auxx);
                                                  if(cc=='L'||cc=='l')
                                                  {
                                                       auxx = auxx+1;
                                                       cc = (*auxx);
                                                       if(cc=='L'||cc=='l')
                                                       {
                                                            auxx = auxx+1;
                                                            cc = (*auxx);
                                                            if(cc=='\0')
                                                            {
                                                                 //printf("DELETE TIENE LAS LETRAS CORRECTAS!\n");
                                                                 params->delet=2;
                                                            }
                                                            else
                                                                 params->delet=-7;
                                                       }
                                                       else
                                                            params->delet=-7;
                                                  }
                                                  else
                                                       params->delet=-7;
                                             }
                                             else
                                                  params->delet=-7;



                                        }
                                        else
                                             params->delet=-7;
                                   }
                                   else
                                        params->error=1;
                              }
                              else
                                   params->error=1;
                         }
                         else
                              params->error=1;
                    }
                    else
                         params->error=1;

               }
               else
                    params->error=1;
          }//fin delete
          else if(c=='N'||c=='n')
          {
               //printf("POSIBLEMENTE SEA UN NAME\n");
               aux = aux+1;
               c = (*aux);
               if(c=='A'||c=='a')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='M'||c=='m')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='E'||c=='e')
                         {
                              aux = aux+1;
                              c = (*aux);
                              if(c=='\0')
                              {


                                   //printf("SI ES UN NAME!\n");
                                   flag=0;
                                   token = strtok(NULL,guion);
                                   //printf("VAL DEL NAME:%s\n",token);

                                   char pathaux[150] = " ";
                                   char* auxx = token;
                                   char cc = (*auxx);
                                   if(cc=='"')
                                   {
                                        //printf("ENTRO A QUE TIENE COMILLAS\n");
                                        auxx = auxx+1;
                                        cc = (*auxx);
                                        int contador=0;

                                        while(cc!='"')
                                        {
                                             //printf("%c",cc);
                                             pathaux[contador]=cc;
                                             contador++;
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                        }
                                        //printf("\nLO QUE QUEDO EN PATHAUX:%s\n",pathaux);
                                        sprintf(params->name,pathaux);

                                   }
                                   else
                                   {
                                        //printf("ENTRO A QUE NO TIENE COMILLAS\n");
                                        int contador=0;

                                        while(cc!=' ' &&cc!='\n')
                                        {
                                             //printf("%c",cc);
                                             pathaux[contador]=cc;
                                             contador++;
                                             auxx = auxx+1;
                                             cc = (*auxx);
                                        }
                                        //printf("LO QUE QUEDO EN PATHAUX:%s\n",pathaux);
                                        sprintf(params->name,pathaux);
                                        //printf("PASO DEL SPRINTF SIN LAS COMILLAS\n");



                                   }

                              }
                              else
                                   params->error=1;
                         }
                         else
                              params->error=1;
                    }
                    else
                         params->error=1;
               }
               else
                    params->error=1;
          }
          else if(c=='A'||c=='a')
          {
               //printf("POSIBLEMENTE SEA UN ADD\n");
               aux = aux+1;
               c = (*aux);
               if(c=='D'||c=='d')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='D'||c=='d')
                    {
                         aux = aux+1;
                         c = (*aux);
                         if(c=='\0')
                         {
                              //printf("SI ES ADD!\n");
                              token = strtok(NULL,espacio);// se divide hasta encontrar un espacio.
                              int valadd = atoi(token);
                              params->sise = valadd;
                         }
                         else
                              params->error=1;


                    }
                    else
                         params->error=1;
               }
               else
                    params->error=1;
          }
          else if(c=='I'||c=='i')
          {
               //printf("POSIBLEMENTE SEA UN ID\n");
               aux = aux+1;
               c = (*aux);
               if(c=='D'||c=='d')
               {
                    aux = aux+1;
                    c = (*aux);
                    if(c=='\0')
                    {
                         //printf("SI ES UN ID");
                         token = strtok(NULL,espacio);// se divide hasta encontrar un espacio.
                         sprintf(params->id,token);
                    }
                    else
                         params->error=1;

               }
               else
                    params->error=1;
          }
          else
               params->error=1;

          token = strtok(NULL,igual);
          //printf("..LLEGO AL FINAL DEL WHILE..\n");

    }

    //printf("fin getparametro\n");

     return params;
}



char* analizador(char comando[1024],LMOUNT *lista)
{
    // char ss[2] = "=";
     int tipo = AnalizarComando(comando);


     switch(tipo)
     {
          case 1:
          {
               //ř(mkdisk)
               int i = 6;
               while(comando[i]!='\0')
               {
                    parametros[i-6] = comando[i];
                    i++;
               }











               Param* param = getParamentros(parametros);
               printf("SIZE DESDE ANALIZADOR:%i.\n",param->sise);
               printf("PATH DEL ANALIZADOR:%s.\n",param->path);
               printf("UNIT DEL ANALIZADOR:%c.\n",param->unit);
               printf("TYPE DEL ANALIZADOR:%c.\n",param->type);
               printf("FIT DEL ANALIZADOR:%s.\n",param->fit);
               printf("DELET DEL ANALIZADOR:%d.\n",param->delet);
               printf("NAME DEL ANALIZADOR:%s.\n",param->name);
               printf("ADD DEL ANALIZADOR:%d.\n",param->add);
               printf("ID DEL ANALIZADOR:%s.\n",param->id);
               printf("ERROR DEL ANALIZADOR:%d.\n",param->error);
               if(param->error==0)
               {
                    //No hay Errores en las palabras reservadas
                    if(strcmp(param->path,"aa")!=0 && param->sise!=-777777)
                    {
                         printf("%c.\n",param->type);
                         printf("%i.\n",param->delet);
                         printf("%i.\n",param->add);
                         //No falta ningun parametro obligatorio;
                         if(param->type=='a'&&param->delet==0&&param->add==-777777&&strcmp(param->fit,"aa")==0&&strcmp(param->name,"aa")==0&&strcmp(param->id,"aa")==0)
                         {
                              if(param->sise>0)
                              {
                                   if(param->unit!='e')
                                   {
                                        printf("ENTRO A CREAR EL DISCO\n");
                                        int ip=execmkdisk(param);
                                        printf("RETORNO DEL EXECMKDISK:%i\n",ip);
                                        //leerMBR(param->path);
                                        //Aqui va el codigo
                                   }
                                   else
                                        return "ERROR CON EL PARAMETRO UNIT!\n";

                              }
                              else
                                   return "ERROR CON EL PARAMETRO SIZE\n";

                         }
                         else
                              return "ERROR,SE INTRODUJO UN PARAMETRO QUE NO PERTENECE AL COMANDO.\n";
                    }
                    else
                         return "ERROR,FALTA UN PARAMETRO OBLIGATORIO.\n";
               }
               else
                    return "ERROR, UN PARAMETRO ES DESCONOCIDO.\n";




               break;
          }
          case 2:
          {
               //(rmdisk)

               int i = 6;
               while(comando[i]!='\0')
               {
                    parametros[i-6] = comando[i];
                    i++;
               }
               Param* param = getParamentros(parametros);
               if(param->error==0)
               {
                    if(strcmp(param->path,"aa")!=0)
                    {
                         if(param->type=='p'&&param->delet==0&&param->add==0&& param->sise==-777777&&param->unit=='m'&&strcmp(param->fit,"WF")==0&&strcmp(param->name,"aa")==0&&strcmp(param->id,"aa")==0)
                         {
                              execrmdisk(param);
                         }
                    }
               }
               break;
          }
          case 3:
          {
               //fdisk
               int i = 5;
               while(comando[i]!='\0')
               {
                    parametros[i-5] = comando[i];
                    i++;
               }
               Param* param = getParamentros(parametros);
               printf("SIZE DESDE ANALIZADOR:%i.\n",param->sise);
               printf("PATH DEL ANALIZADOR:%s.\n",param->path);
               printf("UNIT DEL ANALIZADOR:%c.\n",param->unit);
               printf("TYPE DEL ANALIZADOR:%c.\n",param->type);
               printf("FIT DEL ANALIZADOR:%s.\n",param->fit);
               printf("DELET DEL ANALIZADOR:%d.\n",param->delet);
               printf("NAME DEL ANALIZADOR:%s.\n",param->name);
               printf("ADD DEL ANALIZADOR:%d.\n",param->add);
               printf("ID DEL ANALIZADOR:%s.\n",param->id);
               printf("ERROR DEL ANALIZADOR:%d.\n",param->error);
               int reso = execfdisk(param);
               printf("RETORNO DEL FDISK:%d\n",reso);


               break;
          }
          case 4:
          {
               //(mount)
               int i = 5;
               while(comando[i]!='\0')
               {
                    parametros[i-5] = comando[i];
                    i++;
               }
               Param* param = getParamentros(parametros);
               printf("SIZE DESDE ANALIZADOR:%i.\n",param->sise);
               printf("PATH DEL ANALIZADOR:%s.\n",param->path);
               printf("UNIT DEL ANALIZADOR:%c.\n",param->unit);
               printf("TYPE DEL ANALIZADOR:%c.\n",param->type);
               printf("FIT DEL ANALIZADOR:%s.\n",param->fit);
               printf("DELET DEL ANALIZADOR:%d.\n",param->delet);
               printf("NAME DEL ANALIZADOR:%s.\n",param->name);
               printf("ADD DEL ANALIZADOR:%d.\n",param->add);
               printf("ID DEL ANALIZADOR:%s.\n",param->id);
               printf("ERROR DEL ANALIZADOR:%d.\n",param->error);
               execmontar(param,lista);
               break;
          }
          case 5:
          {
               //unmount
               int i = 7;
               while(comando[i]!='\0')
               {
                    parametros[i-7] = comando[i];
                    i++;
               }
               break;
          }
          case 6:
          {
               //rep
               printf("ENTRO EN EL CASE DE REP\n");

               int i = 3;
               while(comando[i]!='\0')
               {
                    parametros[i-3] = comando[i];
                    i++;
               }
               Param* param = getParamentros(parametros);
               printf("SIZE DESDE ANALIZADOR:%i.\n",param->sise);
               printf("PATH DEL ANALIZADOR:%s.\n",param->path);
               printf("UNIT DEL ANALIZADOR:%c.\n",param->unit);
               printf("TYPE DEL ANALIZADOR:%c.\n",param->type);
               printf("FIT DEL ANALIZADOR:%s.\n",param->fit);
               printf("DELET DEL ANALIZADOR:%d.\n",param->delet);
               printf("NAME DEL ANALIZADOR:%s.\n",param->name);
               printf("ADD DEL ANALIZADOR:%d.\n",param->add);
               printf("ID DEL ANALIZADOR:%s.\n",param->id);
               printf("ERROR DEL ANALIZADOR:%d.\n",param->error);

               if(param->name[0]=='m'||param->name[0]=='M')
               {

               }
               else if(param->name[0]=='d'||param->name[0]=='D')
               {
                    if(param->name[1]=='i'||param->name[1]=='I')
                    {
                         if(param->name[2]=='s'||param->name[2]=='S')
                         {
                              if(param->name[3]=='k'||param->name[3]=='K')
                              {
                                   //if(param->name[4]==' '||param->name[0]=='\0')
                                   //{
                                        execrep(param,lista);
                                  // }

                              }
                         }
                    }
               }
               break;
          }
          case 7:
          {
               //exec
               int i = 4;
               while(comando[i]!='\0')
               {
                    parametros[i-4] = comando[i];
                    i++;
               }
               break;
          }
          case 8:
          {
               //exec
               int i = 4;
               while(comando[i]!='\0')
               {
                    parametros[i-4] = comando[i];
                    i++;
               }
               Param*param = getParamentros(parametros);
               leerMBR(param->path);
               break;
          }
          case 9:
          {
               leerLM(lista);
          }
          case -7:
          {
               break;
          }
     }
     return "fin analizador\n";

}


int execrmdisk(Param* params)
{
     if(remove(params->path)==0) // Eliminamos el archivo
     {
          printf("El disco fue eliminado correctamente\n");
          return 0;
     }
     else
     {
          printf("No se pudo eliminar el disco\n");
          return 1;
     }

}



int execmkdisk(Param* params)
{

     printf("EN EXECMKDISK\n");
     srand(time(NULL));
     char url3[512]="";
     strcpy(url3,params->path);
     crearDirectorio(url3);



     if(params->unit=='k')
     {
          printf("SE VA A CREAR UN DISCO DE %i%c.\n",params->sise,params->unit);
          int tamenbytes = params->sise*1024;
          MBR* mbrr = crearMbr(tamenbytes,rand());
          FILE *fa;
          fa = fopen(params->path, "rb");          // Este modo permite leer y escribir
          if(!fa)
          {
               //No existe,Esta bien!
               fa = fopen(params->path, "w+b");  // si el fichero no existe, lo crea.


               char caracter='\0';

               int i=params->sise;
               for(;i>0;i--)
               {
                    fwrite(&caracter,sizeof(char),1024,fa);
                    fflush(fa);
               }



               fseek(fa, 0, SEEK_SET);
               fwrite(mbrr, sizeof(MBR), 1, fa);
               fflush(fa);

               return fclose(fa);

          }
          else
          {
               printf("YA EXISTE UN DISCO CON ESE NOMBRE!\n");
               return 1;
          }

     }
     else if(params->unit=='m')
     {
          printf("SE VA A CREAR UN DISCO DE %i%c.\n",params->sise,params->unit);
          int tamenbytes = params->sise*1024*1024;
          MBR* mbrr = crearMbr(tamenbytes,rand());
          FILE *fa;
          fa = fopen(params->path, "rb");          // Este modo permite leer y escribir
          if(!fa)
          {
               //No existe,Esta bien!
               fa = fopen(params->path, "w+b");  // si el fichero no existe, lo crea.
               //char caracter='\0';
               //int i=0;
               //for(;i<1024*params->sise;i++)
               //{
                    //printf("%d\n",size);
                    //fwrite(&caracter,sizeof(char),1024,fa);
                    //fflush(fa);
               //}
               //fseek(fa, 0, SEEK_SET);
               //fwrite(mbrr, sizeof(MBR), 1, fa);
               //fflush(fa);
               int tamm = params->sise*1024*1024;
               char* carac = (char*)calloc(tamm,sizeof(char));
               fwrite(carac,tamm,1,fa);
               fflush(fa);
               fseek(fa, 0, SEEK_SET);
               fwrite(mbrr, sizeof(MBR), 1, fa);
               fflush(fa);
               return fclose(fa);

          }
          else
          {
               printf("YA EXISTE EL DISCO!");
               return 1;
          }
   }
   else
   {
     return 1;
   }

}


int execfdisk(Param* params)
{
     printf("\n EN EXECFDISK:\n");
     printf("SIZE DESDE ANALIZADOR:%i.\n",params->sise);
               printf("PATH DEL ANALIZADOR:%s.\n",params->path);
               printf("UNIT DEL ANALIZADOR:%c.\n",params->unit);
               printf("TYPE DEL ANALIZADOR:%c.\n",params->type);
               printf("FIT DEL ANALIZADOR:%s.\n",params->fit);
               printf("DELET DEL ANALIZADOR:%d.\n",params->delet);
               printf("NAME DEL ANALIZADOR:%s.\n",params->name);
               printf("ADD DEL ANALIZADOR:%d.\n",params->add);
               printf("ID DEL ANALIZADOR:%s.\n",params->id);
               printf("ERROR DEL ANALIZADOR:%d.\n",params->error);
//-1=Comando desconocido.
//-2=falto un parametro obligatorio.
//-3 error en unit
//-4 error en type
//-5 error en fit
//-6 error en delete
//-7 error con el disco
     if(!params->error)
     {
          if(strcmp(params->path,"aa")!=0)
          {
               //hay 3 posibles; crear particion,eliminar particion,agregar y quitar espacio de una particion

               if(params->delet!=0)//es delete
               {
                    return 1;
               }
               else if(params->add!=-777777)//es add
               {
                    return 1;
               }
               else// es creacion de particion
               {
                    if(params->sise!=-777777)
                    {
                         if(params->unit!='e')
                         {
                              if(params->unit=='a')
                              {
                                   params->unit='k';
                              }
                              if(params->type!='e')
                              {
                                   if(params->type=='a')
                                   {
                                        params->type='P';
                                   }
                                   if(strcmp(params->fit,"ee")!=0)
                                   {
                                        if(strcmp(params->fit,"aa")==0)
                                        {
                                             sprintf(params->fit,"WF");
                                        }
                                        if(strcmp(params->name,"aa")!=0)
                                        {

                                             //aqui ya se puede crear la particion.


                                             FILE *fa;
                                             MBR mbrr;
                                             fa = fopen(params->path, "r+b");          // Este modo permite leer y escribir
                                             if(fa)
                                             {
                                                  printf("SI EXISTE EL DISCO EL CUAL SE QUIERE PARTICIONAR\n");
                                                  //existe,Esta bien!
                                                  fseek(fa,0,SEEK_SET);
                                                  fread(&mbrr, sizeof(MBR), 1, fa);
                                                  //hay que revisar que no existe una particion con el mismo nombre
                                                  if(!ParticionExiste(&mbrr,params->name))
                                                  {
                                                       //NO EXISTE PARTICION
                                                       printf("NO EXISTE PARTICION\n");

                                                       //hay que revisar si hay espacio en el disco.
                                                       if(!DiscoLLeno(&mbrr))
                                                       {
                                                            printf("NO ESTA LLENO EL DISCO\n");
                                                            //hay que revisar las restricciones de particiones
                                                            if(params->type=='E')
                                                            {
                                                                 if(!TieneExtendida(&mbrr))
                                                                 {
                                                                      //No tiene extendia, se crea


                                                                      fclose(fa);
                                                                      return 0;

                                                                 }
                                                                 else
                                                                 {
                                                                      printf("NO SE PUEDE CREAR PARTICION EXTENDIDA,YA EXISTE UNA EXTENDIDA\n");
                                                                      fclose(fa);
                                                                      return 1;
                                                                 }
                                                            }
                                                            else if(params->type=='L')
                                                            {
                                                                 if(TieneExtendida(&mbrr))
                                                                 {
                                                                      //Si tiene extendida, se procede al proceso de crear logica
                                                                      fclose(fa);
                                                                      return 0;
                                                                 }
                                                                 else
                                                                 {
                                                                      fclose(fa);
                                                                      printf("ERROR, NO HAY EXTENDIDA PARA CREAR UNA LOGICA\n");
                                                                      return 1;
                                                                 }
                                                            }
                                                            else if(params->type=='P')
                                                            {
                                                                 printf("ENTRO A  type = P");

                                                                 CreaParticion(&mbrr,params,fa);
                                                                 return 0;

                                                            }
                                                            else
                                                            {
                                                                 printf("ERROR,EL TIPO ES ERRONEO!\n");
                                                                 fclose(fa);
                                                                 return 1;

                                                            }
                                                       }
                                                       else
                                                       {
                                                            printf("ERROR!,DISCO LLENO");
                                                            fclose(fa);
                                                            return 1;
                                                       }

                                                  }
                                                  else
                                                  {
                                                       printf("ERROR AL CREAR PARTICION!, YA EXISTE UNA CON ESE NOMBRE!");
                                                       fclose(fa);
                                                       return 1;
                                                  }



                                             }
                                             else
                                             {
                                                  printf("ERROR AL CREAR PARTICION(NO EXISTE DISCO!)");
                                                  return 1;
                                             }

                                        }
                                        else
                                             return -2;

                                   }
                                   else
                                        return -5;
                             }
                             else
                                   return -4;
                         }
                         else
                              return -3;
                    }
                    else
                         return -2;
               }//no lleva nada
          }
          else
               return -2;







     }
     else
          return -1;


}//finExecFdisk


void execmontar(Param* params,LMOUNT *lista)
{
    if(!lista)
     printf("LA LISTA ES NULA\n");

     AddInLM(lista,params);
}

void execrep(Param* params,LMOUNT *lista)
{

     printf("\n EN EXEC REP\n");
     if(!lista)
     {
          printf("MIERDA!!, algo esta mal, LA LISTA ES NULA, YA MONTASTE ALGUN DISCO?\n");

     }
     else
     {
          DMOUNT* disco =  BuscarEnLM(lista,params);
          if(disco)
               GrafDisco(params,disco->path);
          else
               printf("ERROR EN MONTAR\n");
     }
}


