#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
typedef struct Partition
{

     char part_status;//indica si la particion esta activa o no
	char part_type;//indica tipo de particion;primaria o extendiada. P o E
	char part_fit;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
	int part_start;// Indica en que byte del disco unicia la particion.
	int part_size;//Contiene el tamano total de la particion en bytes
	char part_name[16];


}PARTITION;


typedef struct mbr
{
    int mbr_tamano;// tam del disco en bytes.
    time_t mbr_fecha_creacion;//Fecha y hora de creacion del disco.
    int mbr_disk_signature;//Numero random q identifica de forma unica a cada disco.
    PARTITION mbr_partition_1;//Estructura con la informacion de la particion 1.
    PARTITION mbr_partition_2;//Estructura con la informacion de la particion 2.
    PARTITION mbr_partition_3;//Estructura con la informacion de la particion 3.
    PARTITION mbr_partition_4;//Estructura con la informacion de la particion 4.

}MBR;

typedef struct ebr
{
     char part_status;
     char part_fit;
     int part_start;
     int part_size;
     int part_next;
     char part_name[16];
}EBR;

typedef struct param
{
     int sise;
     char unit;
     char path[150];
     char type;
     char fit[3];
     int  delet;//1=fast,2=full
     char name[50];
     int add;
     char id[15];
     int error;
}Param;

typedef struct nmount
{

     char name[50];
     char id[5];
     struct nmount*siguiente;
     struct nmount*anterior;
}NMOUNT;

typedef struct lnmount
{
     NMOUNT*primero;
     NMOUNT*ultimo;
     int cont;
}LNMOUNT;

typedef struct dmount
{
     char path[150];
     char letter;
     LNMOUNT *lnmount;
     struct dmount* siguiente;
     struct dmount* anterior;

}DMOUNT;

typedef struct lmount
{
     char lact;
     DMOUNT *primero;
     DMOUNT *ultimo;


}LMOUNT;
//---METODOS PARA LA LISTA DE MONTURAS
DMOUNT* crearDmount(Param* params,char letter);

int LMVacia(LMOUNT* lista);

DMOUNT* BuscarDmount(LMOUNT *lista,Param *params);
int BuscarEnLNM(LNMOUNT*lista,char* nombre);
void AddInLM(LMOUNT *lista,Param* params);
void AddInLNM(LNMOUNT *lista,Param* params,char letter);

DMOUNT* BuscarEnLM(LMOUNT *lista,Param* params);

void leerLM(LMOUNT *lista);



//--


Param* crearNParam();
//-------
MBR* crearMbr(int tam,int signature);
//------
PARTITION* crearPartition();
//------
EBR* crearEbr();
//----

void leerMBR(char* path);


//hay que revisar que no existe una particion con el mismo nombre
int ParticionExiste(MBR* mbrr,char*nombre);

//hay que revisar las restricciones de particiones
int DiscoLLeno(MBR* mbrr);
int TieneExtendida(MBR* mbrr);
//hay que revisar si hay espacio en el disco.


//crea Particion
int CreaParticion(MBR* mbrr,Param* params,FILE* fa);//?

void CorrerParticiones(MBR* mbrr,int nivel);

void GrabarParticion(MBR* mbrr,int val,Param* params,FILE *fa,int flag);



void GrafDisco(Param*params,char*pathdisco);
void GrafMbr(Param*params,char*pathdisco);
void crearDirectorio(char path[512]);
void crearArchivo(char path[512]);


