#include "Estructuras.h"

Param* crearNParam()
{
     Param* nodo;
     nodo = (Param*)calloc(1,sizeof(Param));
     nodo->sise=-777777;
     nodo->unit = 'a';
     sprintf(nodo->path,"aa");
     nodo->type = 'a';
     sprintf(nodo->fit,"aa") ;
     nodo->delet=0;
     sprintf(nodo->name,"aa");
     nodo->add = -777777;
     sprintf(nodo->id,"aa");
     nodo->error=0;
     return nodo;

}
//crearNParam ok

MBR* crearMbr(int tam,int signature)
{
     MBR* mmbr;

     mmbr = (MBR*)calloc(1,sizeof(MBR));
     mmbr->mbr_tamano = tam;
     mmbr->mbr_disk_signature = signature;

     time (&mmbr->mbr_fecha_creacion);


     mmbr->mbr_partition_1.part_status='0';
     mmbr->mbr_partition_2.part_status='0';
     mmbr->mbr_partition_3.part_status='0';
     mmbr->mbr_partition_4.part_status='0';

     return mmbr;

}
//crarMbr ok

PARTITION* crearPartition()
{
     PARTITION* part;
     part = (PARTITION*)calloc(1,sizeof(PARTITION));
     part->part_status='0';
     return part;
}

EBR* crearEbr()
{
     EBR *resp;
     resp = (EBR*)calloc(1,sizeof(EBR));
     return resp;
}
//ok

void leerMBR(char* path)
{
     printf("\n LECTURA DEL MBR:%s\n",path);
     FILE *fa;
     MBR mbrr;
     fa = fopen(path, "rb");          // Este modo permite leer y escribir
     if(fa)
     {
          //existe,Esta bien!
          fseek(fa,0,SEEK_SET);
          fread(&mbrr, sizeof(MBR), 1, fa);
          printf("TAM MBR:%d\n",mbrr.mbr_tamano);
          printf("SIGNATURA MBR:%d\n",mbrr.mbr_disk_signature);

          //time_t rawtime;
          struct tm * timeinfo;
          timeinfo = localtime ( &mbrr.mbr_fecha_creacion);
          char* fec= asctime(timeinfo);
          printf("FECHA MBR:%s\n",fec);
          printf("PARTICIONES:\n");
          if(mbrr.mbr_partition_1.part_status=='1')
          {
               printf("PARTICION1:\n");
               printf("  NAME:%s\n",mbrr.mbr_partition_1.part_name);
               printf("  STATUS:%c\n",mbrr.mbr_partition_1.part_status);
               printf("  TYPE:%c\n",mbrr.mbr_partition_1.part_type);
               printf("  FIT:%c\n",mbrr.mbr_partition_1.part_fit);
               printf("  START:%d\n",mbrr.mbr_partition_1.part_start);
               printf("  SIZE:%d\n",mbrr.mbr_partition_1.part_size);
               if(mbrr.mbr_partition_1.part_type=='E')
               {
                    printf("  PARTICIONES LOGICAS:\n");
                    EBR ebractual;
                    fseek(fa,mbrr.mbr_partition_1.part_start,SEEK_SET);
                    fread(&ebractual,sizeof(EBR),1,fa);
                    if(ebractual.part_status=='1')
                    {
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       STATUS:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
                    else
                         printf("       EL EBR DE DEFAULT ESTA VACIO\n");
                    while(ebractual.part_next!=-1)
                    {
                         fseek(fa,ebractual.part_next,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
               }

          }
          else
               printf("PARTICION1 NO ACTIVA\n");



          if(mbrr.mbr_partition_2.part_status=='1')
          {
               printf("\nPARTICION2:\n");
               printf("  NAME:%s\n",mbrr.mbr_partition_2.part_name);
               printf("  STATUS:%c\n",mbrr.mbr_partition_2.part_status);
               printf("  TYPE:%c\n",mbrr.mbr_partition_2.part_type);
               printf("  FIT:%c\n",mbrr.mbr_partition_2.part_fit);
               printf("  START:%d\n",mbrr.mbr_partition_2.part_start);
               printf("  SIZE:%d\n",mbrr.mbr_partition_2.part_size);

               if(mbrr.mbr_partition_2.part_type=='E')
               {
                    printf("  PARTICIONES LOGICAS:\n");
                    EBR ebractual;
                    fseek(fa,mbrr.mbr_partition_2.part_start,SEEK_SET);
                    fread(&ebractual,sizeof(EBR),1,fa);
                    if(ebractual.part_status=='1')
                    {
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
                    else
                         printf("       EL EBR DE DEFAULT ESTA VACIO\n");
                    while(ebractual.part_next!=-1)
                    {
                         fseek(fa,ebractual.part_next,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
               }

          }
          else
               printf("PARTICION2 NO ACTIVA\n");



          if(mbrr.mbr_partition_3.part_status=='1')
          {
               printf("PARTICION3:\n");
               printf("  NAME:%s\n",mbrr.mbr_partition_3.part_name);
               printf("  STATUS:%c\n",mbrr.mbr_partition_3.part_status);
               printf("  TYPE:%c\n",mbrr.mbr_partition_3.part_type);
               printf("  FIT:%c\n",mbrr.mbr_partition_3.part_fit);
               printf("  START:%d\n",mbrr.mbr_partition_3.part_start);
               printf("  SIZE:%d\n",mbrr.mbr_partition_3.part_size);

               if(mbrr.mbr_partition_3.part_type=='E')
               {
                    printf("  PARTICIONES LOGICAS:\n");
                    EBR ebractual;
                    fseek(fa,mbrr.mbr_partition_3.part_start,SEEK_SET);
                    fread(&ebractual,sizeof(EBR),1,fa);
                    if(ebractual.part_status=='1')
                    {
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
                    else
                         printf("       EL EBR DE DEFAULT ESTA VACIO\n");
                    while(ebractual.part_next!=-1)
                    {
                         fseek(fa,ebractual.part_next,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
               }

          }
          else
               printf("PARTICION3 NO ACTIVA\n");




          if(mbrr.mbr_partition_4.part_status=='1')
          {
               printf("PARTICION4:\n");
               printf("  NAME:%s\n",mbrr.mbr_partition_4.part_name);
               printf("  STATUS:%c\n",mbrr.mbr_partition_4.part_status);
               printf("  TYPE:%c\n",mbrr.mbr_partition_4.part_type);
               printf("  FIT:%c\n",mbrr.mbr_partition_4.part_fit);
               printf("  START:%d\n",mbrr.mbr_partition_4.part_start);
               printf("  SIZE:%d\n",mbrr.mbr_partition_4.part_size);

               if(mbrr.mbr_partition_4.part_type=='E')
               {
                    printf("  PARTICIONES LOGICAS:\n");
                    EBR ebractual;
                    fseek(fa,mbrr.mbr_partition_4.part_start,SEEK_SET);
                    fread(&ebractual,sizeof(EBR),1,fa);
                    if(ebractual.part_status=='1')
                    {
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
                    else
                         printf("       EL EBR DE DEFAULT ESTA VACIO\n");
                    while(ebractual.part_next!=-1)
                    {
                         fseek(fa,ebractual.part_next,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
               }

          }
          else
               printf("PARTICION1 NO ACTIVA\n");





     fclose(fa);


     }
     else
          printf("ERROR AL LEER MBR");


}//end leerMbr

//ok

//hay que revisar que no existe una particion con el mismo nombre
int ParticionExiste(MBR* mbrr,char* name)
{
     if(mbrr->mbr_partition_1.part_status=='0')
          return 0;
     else
     {
          if(strcmp(mbrr->mbr_partition_1.part_name,name)==0)
               return 1;
          else
          {
               if(mbrr->mbr_partition_2.part_status=='0')
                    return 0;
               else
               {
                    if(strcmp(mbrr->mbr_partition_2.part_name,name)==0)
                    return 1;
                    else
                    {

                         if(mbrr->mbr_partition_3.part_status=='0')
                              return 0;
                         else
                         {
                              if(strcmp(mbrr->mbr_partition_3.part_name,name)==0)
                                   return 1;
                              else
                              {
                                   if(mbrr->mbr_partition_4.part_status=='0')
                                        return 0;
                                   else
                                   {
                                        if(strcmp(mbrr->mbr_partition_4.part_name,name)==0)
                                             return 1;
                                        else
                                             return 0;



                                   }


                              }
                         }


                    }
               }



          }

     }

}//end particionexiste
//ok

//hay que revisar las restricciones de particiones
int DiscoLLeno(MBR* mbrr)
{
     if(mbrr->mbr_partition_1.part_status=='1'&&mbrr->mbr_partition_2.part_status=='1'&&mbrr->mbr_partition_3.part_status=='1'&&mbrr->mbr_partition_4.part_status=='1')
          return 1;
     else
          return 0;
}//fin discolleno
//ok





int TieneExtendida(MBR* mbrr)
{
     if(mbrr->mbr_partition_1.part_status=='1')
     {
          if(mbrr->mbr_partition_1.part_type=='E')
          return 1;
          else
          {
               //**************
               if(mbrr->mbr_partition_2.part_status=='1')
               {
                    if(mbrr->mbr_partition_2.part_type=='E')
                         return 1;
                    else
                    {
                         //***********
                         if(mbrr->mbr_partition_3.part_status=='1')
                         {
                              if(mbrr->mbr_partition_3.part_type=='E')
                                   return 1;
                              else
                              {
                                   //********
                                   if(mbrr->mbr_partition_4.part_status=='1')
                                   {
                                        if(mbrr->mbr_partition_1.part_type=='E')
                                             return 1;
                                        else
                                             return 0;

                                   }
                                   else
                                        return 0;
                                   //*********




                              }
                         }
                         else
                              return 0;
                         //**************

                    }
               }
               else
                    return 0;
               //*******

          }
     }
     else
          return 0;
}//FIN TIENE EXTENDIDA
//ok

void CorrerParticiones(MBR* mbrr,int nivel)
{
     if(nivel==1)
     {
          //se corren todos;
          mbrr->mbr_partition_4.part_status = mbrr->mbr_partition_3.part_status;//indica si la particion esta activa o no
          mbrr->mbr_partition_4.part_type = mbrr->mbr_partition_3.part_type ;//indica tipo de particion;primaria o extendiada. P o E
          mbrr->mbr_partition_4.part_fit = mbrr->mbr_partition_3.part_fit ;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
          mbrr->mbr_partition_4.part_start = mbrr->mbr_partition_3.part_start ;// Indica en que byte del disco unicia la particion.
          mbrr->mbr_partition_4.part_size = mbrr->mbr_partition_3.part_size ;//Contiene el tamano total de la particion en bytes
          sprintf(mbrr->mbr_partition_4.part_name,mbrr->mbr_partition_3.part_name);

          //---
          mbrr->mbr_partition_3.part_status = mbrr->mbr_partition_2.part_status;//indica si la particion esta activa o no
          mbrr->mbr_partition_3.part_type = mbrr->mbr_partition_2.part_type ;//indica tipo de particion;primaria o extendiada. P o E
          mbrr->mbr_partition_3.part_fit = mbrr->mbr_partition_2.part_fit ;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
          mbrr->mbr_partition_3.part_start = mbrr->mbr_partition_2.part_start ;// Indica en que byte del disco unicia la particion.
          mbrr->mbr_partition_3.part_size = mbrr->mbr_partition_2.part_size ;//Contiene el tamano total de la particion en bytes
          sprintf(mbrr->mbr_partition_3.part_name,mbrr->mbr_partition_2.part_name);

          //---

          mbrr->mbr_partition_2.part_status = mbrr->mbr_partition_1.part_status;//indica si la particion esta activa o no
          mbrr->mbr_partition_2.part_type = mbrr->mbr_partition_1.part_type ;//indica tipo de particion;primaria o extendiada. P o E
          mbrr->mbr_partition_2.part_fit = mbrr->mbr_partition_1.part_fit ;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
          mbrr->mbr_partition_2.part_start = mbrr->mbr_partition_1.part_start ;// Indica en que byte del disco unicia la particion.
          mbrr->mbr_partition_2.part_size = mbrr->mbr_partition_1.part_size ;//Contiene el tamano total de la particion en bytes
          sprintf(mbrr->mbr_partition_2.part_name,mbrr->mbr_partition_1.part_name);

     }
     else if(nivel==2)
     {
          //se corren todos;
          mbrr->mbr_partition_4.part_status = mbrr->mbr_partition_3.part_status;//indica si la particion esta activa o no
          mbrr->mbr_partition_4.part_type = mbrr->mbr_partition_3.part_type ;//indica tipo de particion;primaria o extendiada. P o E
          mbrr->mbr_partition_4.part_fit = mbrr->mbr_partition_3.part_fit ;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
          mbrr->mbr_partition_4.part_start = mbrr->mbr_partition_3.part_start ;// Indica en que byte del disco unicia la particion.
          mbrr->mbr_partition_4.part_size = mbrr->mbr_partition_3.part_size ;//Contiene el tamano total de la particion en bytes
          sprintf(mbrr->mbr_partition_4.part_name,mbrr->mbr_partition_3.part_name);

          //---
          mbrr->mbr_partition_3.part_status = mbrr->mbr_partition_2.part_status;//indica si la particion esta activa o no
          mbrr->mbr_partition_3.part_type = mbrr->mbr_partition_2.part_type ;//indica tipo de particion;primaria o extendiada. P o E
          mbrr->mbr_partition_3.part_fit = mbrr->mbr_partition_2.part_fit ;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
          mbrr->mbr_partition_3.part_start = mbrr->mbr_partition_2.part_start ;// Indica en que byte del disco unicia la particion.
          mbrr->mbr_partition_3.part_size = mbrr->mbr_partition_2.part_size ;//Contiene el tamano total de la particion en bytes
          sprintf(mbrr->mbr_partition_3.part_name,mbrr->mbr_partition_2.part_name);



     }
     else if(nivel==3)
     {
          //se corren todos;
          mbrr->mbr_partition_4.part_status = mbrr->mbr_partition_3.part_status;//indica si la particion esta activa o no
          mbrr->mbr_partition_4.part_type = mbrr->mbr_partition_3.part_type ;//indica tipo de particion;primaria o extendiada. P o E
          mbrr->mbr_partition_4.part_fit = mbrr->mbr_partition_3.part_fit ;//Tipo de ajuste de la particion. B(best), F(first) o W(worst)
          mbrr->mbr_partition_4.part_start = mbrr->mbr_partition_3.part_start ;// Indica en que byte del disco unicia la particion.
          mbrr->mbr_partition_4.part_size = mbrr->mbr_partition_3.part_size ;//Contiene el tamano total de la particion en bytes
          sprintf(mbrr->mbr_partition_4.part_name,mbrr->mbr_partition_3.part_name);



     }

}//end correrparticiones


void GrabarParticion(MBR* mbrr,int val,Param* params,FILE *fa,int flag)
{
     switch(val)
     {
          case 1:
          {
               mbrr->mbr_partition_1.part_status='1';
               mbrr->mbr_partition_1.part_type=params->type;
               if(strcmp(params->fit,"WF")==0)
                    mbrr->mbr_partition_1.part_fit='W';
               else if(strcmp(params->fit,"BF")==0)
                    mbrr->mbr_partition_1.part_fit='B';
               else if(strcmp(params->fit,"FF")==0)
                    mbrr->mbr_partition_1.part_fit='F';
               else
                    printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");

               mbrr->mbr_partition_1.part_start = sizeof(MBR)+1;
               if(params->unit=='k')
               {
                    mbrr->mbr_partition_1.part_size = params->sise*1024;

               }
               else
               {
                    mbrr->mbr_partition_1.part_size = params->sise*1024*1024;
               }

               sprintf(mbrr->mbr_partition_1.part_name,params->name);
               //FILE *fa;
               fa = fopen(params->path, "r+b");          // Este modo permite leer y escribir
               if(fa)
               {
                    printf("\n EN EL GRABARPARTICION SI SE VA IR A GRABAR AL DISCO EL NUEVO MBR!!!\n");
                    fseek(fa, 0, SEEK_SET);
                    fwrite(mbrr, sizeof(MBR), 1, fa);
                    fflush(fa);
                    if(flag)
                    {
                         fseek(fa,(sizeof(MBR)+1),SEEK_SET);
                         EBR *ebrr = crearEbr();
                         ebrr->part_status='0';
                         ebrr->part_next=-1;
                         ebrr->part_start=mbrr->mbr_partition_1.part_start+sizeof(EBR)+1;
                         fwrite(ebrr,sizeof(EBR),1,fa);
                    }

                    fclose(fa);
               }
               else
               {
                    printf("OCURRIO UN PROBLEMA A LA HORA DE REESCRIBIR EL MBR");
               }
               break;
          }
          case 2:
          {
               int start = mbrr->mbr_partition_1.part_start+mbrr->mbr_partition_1.part_size+1;

               mbrr->mbr_partition_2.part_status='1';
               mbrr->mbr_partition_2.part_type=params->type;
               if(strcmp(params->fit,"WF")==0)
                    mbrr->mbr_partition_2.part_fit='W';
               else if(strcmp(params->fit,"BF")==0)
                    mbrr->mbr_partition_2.part_fit='B';
               else if(strcmp(params->fit,"FF")==0)
                    mbrr->mbr_partition_2.part_fit='F';
               else
                    printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");

               mbrr->mbr_partition_2.part_start = start;
               if(params->unit=='k')
               {
                    mbrr->mbr_partition_2.part_size = params->sise*1024;

               }
               else
               {
                    mbrr->mbr_partition_2.part_size = params->sise*1024*1024;
               }

               sprintf(mbrr->mbr_partition_2.part_name,params->name);
               //FILE *fa;
               fa = fopen(params->path, "r+b");          // Este modo permite leer y escribir
               if(fa)
               {
                    printf("\n EN EL GRABARPARTICION SI SE VA IR A GRABAR AL DISCO EL NUEVO MBR!!!\n");
                    fseek(fa, 0, SEEK_SET);
                    fwrite(mbrr, sizeof(MBR), 1, fa);
                    fflush(fa);
                    if(flag)
                    {
                         fseek(fa,start,SEEK_SET);
                         EBR *ebrr = crearEbr();
                         ebrr->part_status='0';
                         ebrr->part_next=-1;
                         ebrr->part_start=mbrr->mbr_partition_2.part_start+sizeof(EBR)+1;
                         fwrite(ebrr,sizeof(EBR),1,fa);
                    }
                    fclose(fa);
               }
               else
               {
                    printf("OCURRIO UN PROBLEMA A LA HORA DE REESCRIBIR EL MBR");
               }
               break;


          }
          case 3:
          {
               int start = mbrr->mbr_partition_2.part_start+mbrr->mbr_partition_2.part_size+1;

               mbrr->mbr_partition_3.part_status='1';
               mbrr->mbr_partition_3.part_type=params->type;
               if(strcmp(params->fit,"WF")==0)
                    mbrr->mbr_partition_3.part_fit='W';
               else if(strcmp(params->fit,"BF")==0)
                    mbrr->mbr_partition_3.part_fit='B';
               else if(strcmp(params->fit,"FF")==0)
                    mbrr->mbr_partition_3.part_fit='F';
               else
                    printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");

               mbrr->mbr_partition_3.part_start = start;
               if(params->unit=='k')
               {
                    mbrr->mbr_partition_3.part_size = params->sise*1024;

               }
               else
               {
                    mbrr->mbr_partition_3.part_size = params->sise*1024*1024;
               }

               sprintf(mbrr->mbr_partition_3.part_name,params->name);
               //FILE *fa;
               fa = fopen(params->path, "r+b");          // Este modo permite leer y escribir
               if(fa)
               {
                    printf("\n EN EL GRABARPARTICION SI SE VA IR A GRABAR AL DISCO EL NUEVO MBR!!!\n");
                    fseek(fa, 0, SEEK_SET);
                    fwrite(mbrr, sizeof(MBR), 1, fa);
                    fflush(fa);
                    if(flag)
                    {
                         fseek(fa,start,SEEK_SET);
                         EBR *ebrr = crearEbr();
                         ebrr->part_status='0';
                         ebrr->part_next=-1;
                         ebrr->part_start=mbrr->mbr_partition_3.part_start+sizeof(EBR)+1;
                         fwrite(ebrr,sizeof(EBR),1,fa);
                    }

                    fclose(fa);
               }
               else
               {
                    printf("OCURRIO UN PROBLEMA A LA HORA DE REESCRIBIR EL MBR");
               }
               break;










          }
          case 4:
          {
               int start = mbrr->mbr_partition_3.part_start+mbrr->mbr_partition_3.part_size+1;

               mbrr->mbr_partition_4.part_status='1';
               mbrr->mbr_partition_4.part_type=params->type;
               if(strcmp(params->fit,"WF")==0)
                    mbrr->mbr_partition_4.part_fit='W';
               else if(strcmp(params->fit,"BF")==0)
                    mbrr->mbr_partition_4.part_fit='B';
               else if(strcmp(params->fit,"FF")==0)
                    mbrr->mbr_partition_4.part_fit='F';
               else
                    printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");

               mbrr->mbr_partition_4.part_start = start;
               if(params->unit=='k')
               {
                    mbrr->mbr_partition_4.part_size = params->sise*1024;

               }
               else
               {
                    mbrr->mbr_partition_4.part_size = params->sise*1024*1024;
               }

               sprintf(mbrr->mbr_partition_4.part_name,params->name);
               //FILE *fa;
               fa = fopen(params->path, "r+b");          // Este modo permite leer y escribir
               if(fa)
               {
                    printf("\n EN EL GRABARPARTICION SI SE VA IR A GRABAR AL DISCO EL NUEVO MBR!!!\n");
                    fseek(fa, 0, SEEK_SET);
                    fwrite(mbrr, sizeof(MBR), 1, fa);
                    fflush(fa);
                    if(flag)
                    {
                         fseek(fa,start,SEEK_SET);
                         EBR *ebrr = crearEbr();
                         ebrr->part_status='0';
                         ebrr->part_next=-1;
                         ebrr->part_start=mbrr->mbr_partition_4.part_start+sizeof(EBR)+1;
                         fwrite(ebrr,sizeof(EBR),1,fa);
                    }
                    fclose(fa);
               }
               else
               {
                    printf("OCURRIO UN PROBLEMA A LA HORA DE REESCRIBIR EL MBR");
               }
               break;

          }

     }
}//FINGrabarParcticion






//crea Particion
int CreaParticion(MBR* mbrr,Param* params,FILE *fa)
{

     if(!DiscoLLeno(mbrr))
     {
          int realsize;
          if(params->unit=='k')
          {
               realsize = params->sise*1024;

          }
          else
          {
               realsize = params->sise*1024*1024;
          }

          if(params->type=='P')
          {
               if(mbrr->mbr_partition_1.part_status=='1')
               {
                    printf("ENTRO A QUE LA PARTICION1 YA ESTA OCUPADA\n ");


                    int backpart1=mbrr->mbr_partition_1.part_start-(sizeof(MBR)+1)-1;//aqui verificamos si no hay espacio entre el mbr y la particion1
                    if(backpart1>realsize)
                    {
                         // si cabe la particion que se quiere hacer
                         //se hace la particion..[SOLO SE CORREN EN LA TABLA DEL MBR]
                         CorrerParticiones(mbrr,1);
                         //se agrega a la particion1
                         GrabarParticion(mbrr,1,params,fa,0);
                         return 0;


                    }
                    else if(mbrr->mbr_partition_2.part_status=='0')
                    {
                         //esta ocupada la particion 1 pero la 2 no y  no hay espacio entre el MBR y la particion1!
                         int posible = mbrr->mbr_partition_1.part_start+mbrr->mbr_partition_1.part_size+1+realsize;
                         if(posible<mbrr->mbr_tamano)
                         {
                              //Si cabe La particion todavia
                              printf("SE VA A GRABAR LA PARTICION2 CON:%s\n",params->name);
                              GrabarParticion(mbrr,2,params,fa,0);
                              return 0;
                         }
                         else
                         {
                              printf("ERROR,NO HAY SUFICIENTE ESPACIO!\n");
                              return 1;
                         }

                    }
                    else if(mbrr->mbr_partition_2.part_status=='1')
                    {
                         //esta ocupado la particion 1 y 2
                         int spacebetween1y2 = mbrr->mbr_partition_2.part_start - mbrr->mbr_partition_1.part_size - mbrr->mbr_partition_1.part_start-1;
                         if(spacebetween1y2>realsize)
                         {
                              //Si cabe la particion en medio de la 1 y 2
                              CorrerParticiones(mbrr,2);
                              GrabarParticion(mbrr,2,params,fa,0);
                              return 0;

                         }
                         else
                         {

                              //esta ocupado la particion1
                              //no hay espacio entre el mbr y p1
                              //esta ocupada la p2
                              //no cabe la particion entre la 1 y 2
                              if(mbrr->mbr_partition_3.part_status=='0')
                              {
                                   //esta vacio la particion3
                                   int posiblep3  = mbrr->mbr_partition_2.part_start+mbrr->mbr_partition_2.part_size+1+realsize;
                                   if(posiblep3<mbrr->mbr_tamano)
                                   {
                                        //Si cabe La particion todavia
                                        printf("SE VA A GRABAR LA PARTICION3 CON:%s\n",params->name);
                                        GrabarParticion(mbrr,3,params,fa,0);
                                        return 0;
                                   }
                                   else
                                   {
                                        printf("ERROR,NO HAY SUFICIENTE ESPACIO!\n");
                                        return 1;
                                   }


                              }
                              else if(mbrr->mbr_partition_3.part_status=='1')
                              {
                                   //esta ocupada la p3
                                   int spacebetween2y3 = mbrr->mbr_partition_3.part_start - mbrr->mbr_partition_2.part_size - mbrr->mbr_partition_2.part_start-1;
                                   if(spacebetween2y3>realsize)
                                   {
                                        //Si cabe la particion en medio de la 1 y 2
                                        CorrerParticiones(mbrr,3);
                                        GrabarParticion(mbrr,3,params,fa,0);
                                        return 0;

                                   }
                                   else
                                   {

                                        //esta ocupado la particion1
                                        //no hay espacio entre el mbr y p1
                                        //esta ocupada la p2
                                        //no cabe la particion entre la 1 y 2
                                        //esta ocupada la p3
                                        //no cabe la particion entre la 2 y 3
                                        if(mbrr->mbr_partition_4.part_status=='0')
                                        {
                                             //esta vacio la particion3
                                             int posiblep4 = mbrr->mbr_partition_3.part_start+mbrr->mbr_partition_3.part_size+1+realsize;
                                             if(posiblep4<mbrr->mbr_tamano)
                                             {
                                                  //Si cabe La particion todavia
                                                  printf("SE VA A GRABAR LA PARTICION4 CON:%s\n",params->name);
                                                  GrabarParticion(mbrr,4,params,fa,0);
                                                  return 0;
                                             }
                                             else
                                             {
                                                  printf("ERROR,NO HAY SUFICIENTE ESPACIO!\n");
                                                  return 1;
                                             }


                                        }
                                        else if(mbrr->mbr_partition_4.part_status=='1')
                                        {
                                             //esta ocupada la p4
                                             printf("ERROR!!! NO DEBERIA DE HABER LLEGADO HASTA ACA!!\n");
                                             return 1;

                                        }
                                        else
                                             return -1;
                                   }

                              }
                              else
                                   return 1;
                         }


                    }
                    else
                         return 1;

               }
               else
               {
                    printf("ENTRO A QUE LA PARTICION1 NO ESTA OCUPADA\n ");
                    GrabarParticion(mbrr,1,params,fa,0);
                    return 0;

               }


          }
//

          else if(params->type=='E')
          {
 //-----------------------------------------------EXTENDIDAS-----------------------------------------------------------------
               if(mbrr->mbr_partition_1.part_status=='1')
               {
                    printf("ENTRO A QUE LA PARTICION1 YA ESTA OCUPADA\n ");


                    int backpart1=mbrr->mbr_partition_1.part_start-(sizeof(MBR)+1)-1;//aqui verificamos si no hay espacio entre el mbr y la particion1
                    if(backpart1>(realsize+sizeof(EBR)))
                    {
                         // si cabe la particion que se quiere hacer
                         //se hace la particion..[SOLO SE CORREN EN LA TABLA DEL MBR]
                         CorrerParticiones(mbrr,1);
                         //se agrega a la particion1
                         GrabarParticion(mbrr,1,params,fa,1);
                         return 0;


                    }
                    else if(mbrr->mbr_partition_2.part_status=='0')
                    {
                         //esta ocupada la particion 1 pero la 2 no y  no hay espacio entre el MBR y la particion1!
                         int posible = mbrr->mbr_partition_1.part_start+mbrr->mbr_partition_1.part_size+1+realsize+sizeof(EBR);
                         if(posible<mbrr->mbr_tamano)
                         {
                              //Si cabe La particion todavia
                              printf("SE VA A GRABAR LA PARTICION2(EBR) CON:%s\n",params->name);
                              GrabarParticion(mbrr,2,params,fa,1);
                              return 0;
                         }
                         else
                         {
                              printf("ERROR,NO HAY SUFICIENTE ESPACIO!\n");
                              return 1;
                         }

                    }
                    else if(mbrr->mbr_partition_2.part_status=='1')
                    {
                         //esta ocupado la particion 1 y 2
                         int spacebetween1y2 = mbrr->mbr_partition_2.part_start - mbrr->mbr_partition_1.part_size - mbrr->mbr_partition_1.part_start-1;
                         if(spacebetween1y2>(realsize+sizeof(EBR)))
                         {
                              //Si cabe la particion en medio de la 1 y 2
                              CorrerParticiones(mbrr,2);
                              GrabarParticion(mbrr,2,params,fa,1);
                              return 0;

                         }
                         else
                         {

                              //esta ocupado la particion1
                              //no hay espacio entre el mbr y p1
                              //esta ocupada la p2
                              //no cabe la particion entre la 1 y 2
                              if(mbrr->mbr_partition_3.part_status=='0')
                              {
                                   //esta vacio la particion3
                                   int posiblep3  = mbrr->mbr_partition_2.part_start+mbrr->mbr_partition_2.part_size+1+realsize+sizeof(EBR);
                                   if(posiblep3<mbrr->mbr_tamano)
                                   {
                                        //Si cabe La particion todavia
                                        printf("SE VA A GRABAR LA PARTICION3 CON:%s\n",params->name);
                                        GrabarParticion(mbrr,3,params,fa,1);
                                        return 0;
                                   }
                                   else
                                   {
                                        printf("ERROR,NO HAY SUFICIENTE ESPACIO!\n");
                                        return 1;
                                   }


                              }
                              else if(mbrr->mbr_partition_3.part_status=='1')
                              {
                                   //esta ocupada la p3
                                   int spacebetween2y3 = mbrr->mbr_partition_3.part_start - mbrr->mbr_partition_2.part_size - mbrr->mbr_partition_2.part_start-1;
                                   if(spacebetween2y3>(realsize+sizeof(EBR)))
                                   {
                                        //Si cabe la particion en medio de la 1 y 2
                                        CorrerParticiones(mbrr,3);
                                        GrabarParticion(mbrr,3,params,fa,1);
                                        return 0;

                                   }
                                   else
                                   {

                                        //esta ocupado la particion1
                                        //no hay espacio entre el mbr y p1
                                        //esta ocupada la p2
                                        //no cabe la particion entre la 1 y 2
                                        //esta ocupada la p3
                                        //no cabe la particion entre la 2 y 3
                                        if(mbrr->mbr_partition_4.part_status=='0')
                                        {
                                             //esta vacio la particion4
                                             int posiblep4 = mbrr->mbr_partition_3.part_start+mbrr->mbr_partition_3.part_size+1+realsize+sizeof(EBR);
                                             if(posiblep4<mbrr->mbr_tamano)
                                             {
                                                  //Si cabe La particion todavia
                                                  printf("SE VA A GRABAR LA PARTICION4 CON:%s\n",params->name);
                                                  GrabarParticion(mbrr,4,params,fa,1);
                                                  return 0;
                                             }
                                             else
                                             {
                                                  printf("ERROR,NO HAY SUFICIENTE ESPACIO!\n");
                                                  return 1;
                                             }


                                        }
                                        else if(mbrr->mbr_partition_4.part_status=='1')
                                        {
                                             //esta ocupada la p4
                                             printf("ERROR!!! NO DEBERIA DE HABER LLEGADO HASTA ACA!!\n");
                                             return 1;

                                        }
                                        else
                                             return -1;
                                   }

                              }
                              else
                                   return 1;
                         }


                    }
                    else
                         return 1;

               }
               else
               {
                    printf("ENTRO A QUE LA PARTICION1 NO ESTA OCUPADA\n ");
                    GrabarParticion(mbrr,1,params,fa,1);
                    return 0;

               }







//--------------------------------------FIN EXTENDIDAS-------------------------------------------------------------------------

          }
          else if(params->type=='L')
          {
//-----------------------------------------LOGICAS---------------------------------------------------------------------
               //SE LOCALIZA LA PARTICION EXTENDIDA
               PARTITION *particion=0;
               if(mbrr->mbr_partition_1.part_status=='1')
               {
                    if(mbrr->mbr_partition_1.part_type=='E')
                         particion = &mbrr->mbr_partition_1;
               }

               if(mbrr->mbr_partition_2.part_status=='1')
               {
                    if(mbrr->mbr_partition_2.part_type=='E')
                         particion = &mbrr->mbr_partition_2;
               }

               if(mbrr->mbr_partition_3.part_status=='1')
               {
                    if(mbrr->mbr_partition_3.part_type=='E')
                         particion = &mbrr->mbr_partition_3;
               }

               if(mbrr->mbr_partition_4.part_status=='1')
               {
                    if(mbrr->mbr_partition_4.part_type=='E')
                         particion = &mbrr->mbr_partition_4;
               }


               if(particion)
               {
                    EBR ebractual;
                    EBR *nuevo;
                    fseek(fa,particion->part_start,SEEK_SET);

                    fread(&ebractual,sizeof(EBR),1,fa);
                    int termino = 0;
                    int limitextend = particion->part_start+particion->part_size;
                    while(!termino)
                    {
                         if(ebractual.part_status=='0')
                         {
                              //ES EL PRIMER EBR EN LA PRIMERA INCERCION DE LOGICA.
                              //ES EL PRIMER EBR EL CUAL FUE BORRADO PERO PUEDE QUE TENGA OTRA PARTICION EXTENDIDA
                              if(ebractual.part_next==-1)
                              {
                                   //NO TIENE NINGUNA OTRA
                                   //SE VERIFICA SI DA EL TAM LA EXTENDIDA
                                   if(ebractual.part_start+ebractual.part_size+realsize<limitextend)
                                   {
                                        //SI CABE LA PARTICION
                                        nuevo = crearEbr();
                                        nuevo->part_status='1';
                                        if(strcmp(params->fit,"WF")==0)
                                             nuevo->part_fit='W';
                                        else if(strcmp(params->fit,"BF")==0)
                                             nuevo->part_fit='B';
                                        else if(strcmp(params->fit,"FF")==0)
                                             nuevo->part_fit='F';
                                        else
                                             printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");
                                        sprintf(nuevo->part_name,params->name);
                                        nuevo->part_next=ebractual.part_next;
                                        nuevo->part_size= realsize;
                                        nuevo->part_start=ebractual.part_start;
                                        printf("ANTES DEL FSEEK\n");
                                        int loqtiene = ebractual.part_start-sizeof(EBR)-1;
                                        printf("LO QUE TIENE FSEEK:%d\n",loqtiene);
                                        fseek(fa,ebractual.part_start-sizeof(EBR)-1,SEEK_SET);
                                        printf("ANTES DEL FWRITE\n");
                                        if(nuevo)
                                             printf("EL NUEVO NO ES NULO!\n");
                                        else
                                             printf("EL NUEVO ES NULO!!!!!!\n");
                                        fwrite(nuevo,sizeof(EBR),1,fa);
                                        termino=1;
                                        free(nuevo);
                                        fclose(fa);


                                   }
                                   else
                                   {
                                        //NO CABE LA PARTICION
                                        printf("ERROR EN CREAR LOGICA.NO HAY SUFICIENTE ESPACIO EN LA PARTICION EXTENDIDA!\n ");
                                        termino=1;
                                        fclose(fa);
                                   }




                              }
                              else
                              {
                                   //TIENE OTRA
                                   //HAY QUE VER SI CABE ATRAS DE LA OTRA
                                   if(realsize<=(ebractual.part_next-ebractual.part_start))
                                   {
                                        //Si cabe para activarla con el primer EBR!
                                        nuevo = crearEbr();
                                        nuevo->part_status='1';
                                        if(strcmp(params->fit,"WF")==0)
                                             mbrr->mbr_partition_4.part_fit='W';
                                        else if(strcmp(params->fit,"BF")==0)
                                             mbrr->mbr_partition_4.part_fit='B';
                                        else if(strcmp(params->fit,"FF")==0)
                                             mbrr->mbr_partition_4.part_fit='F';
                                        else
                                             printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");
                                        sprintf(nuevo->part_name,params->name);
                                        nuevo->part_next=ebractual.part_next;
                                        nuevo->part_size= realsize;
                                        nuevo->part_start=ebractual.part_start;
                                        fseek(fa,ebractual.part_start-sizeof(EBR)-1,SEEK_SET);
                                        fwrite(nuevo,sizeof(EBR),1,fa);
                                        termino=1;
                                        free(nuevo);
                                        fclose(fa);

                                   }
                                   else
                                   {
                                        //NO cabe, sedemos el control al siguiente EBR
                                        fseek(fa,ebractual.part_next,SEEK_SET);
                                        fread(&ebractual,sizeof(EBR),1,fa);
                                   }

                              }


                         }//FIN if(ebractual.part_status=='0')
                         else
                         {
                              //LA ACTUAL ESTA ACTIVADA!
                              //PUEDA QUE TENGA SIGUIENTE O NO!
                              if(ebractual.part_next==-1)
                              {
                                   //NO TIENE SIGUIENTE.
                                   // HAY QUE VERIFICAR SI CABE EN LO QUE QUEDA DEL DISCO

                                   if(ebractual.part_start+ebractual.part_size+realsize+sizeof(EBR)<limitextend)
                                   {
                                        //SI CABE LA PARTICION

                                        //se crea elnuevo EBR
                                        nuevo = crearEbr();
                                        nuevo->part_status='1';
                                        if(strcmp(params->fit,"WF")==0)
                                             mbrr->mbr_partition_4.part_fit='W';
                                        else if(strcmp(params->fit,"BF")==0)
                                             mbrr->mbr_partition_4.part_fit='B';
                                        else if(strcmp(params->fit,"FF")==0)
                                             mbrr->mbr_partition_4.part_fit='F';
                                        else
                                             printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");
                                        sprintf(nuevo->part_name,params->name);
                                        nuevo->part_next=-1;//EL SIGUIENTE DEL NUEVO
                                        nuevo->part_size= realsize;//EL TAM DEL NUEVO LOGICO
                                        nuevo->part_start=ebractual.part_start+ebractual.part_size+1+sizeof(EBR);//LA POSICION DE DONDE EMPIEZA LA LOGICA
                                        //SE ESCRIBE EL NUEVO EBR
                                        fseek(fa,ebractual.part_start+ebractual.part_size+1,SEEK_SET);
                                        fwrite(nuevo,sizeof(EBR),1,fa);
                                        //SE MODIFICA EL EBR ACTUAL CON LA SIGUIENTE POS
                                        ebractual.part_next= ebractual.part_start+ebractual.part_size+1;
                                        //SE SOBREESCRIBE EL EBR ACTUAL
                                        fseek(fa,ebractual.part_start-sizeof(EBR)-1,SEEK_SET);
                                        fwrite(&ebractual,sizeof(EBR),1,fa);

                                        termino=1;
                                        free(nuevo);
                                        fclose(fa);


                                   }
                                   else
                                   {
                                        //NO CABE LA PARTICION
                                        printf("ERROR EN CREAR LOGICA.NO HAY SUFICIENTE ESPACIO EN LA PARTICION EXTENDIDA!\n ");
                                        termino=1;
                                        fclose(fa);
                                   }




                              }
                              else
                              {
                                   //ACTIVA Y SI TIENE SIGUIENTE
                                   //HAY QUE VERIFICAR SI CABE ENTRE LAS 2
                                   int espbetween = ebractual.part_next - (ebractual.part_start+ebractual.part_size)-1;
                                   if(realsize+sizeof(EBR)<espbetween)
                                   {
                                        //SI CABE
                                        //SE CREA EL NUEVO
                                        nuevo = crearEbr();
                                        nuevo->part_status='1';
                                        if(strcmp(params->fit,"WF")==0)
                                             mbrr->mbr_partition_4.part_fit='W';
                                        else if(strcmp(params->fit,"BF")==0)
                                             mbrr->mbr_partition_4.part_fit='B';
                                        else if(strcmp(params->fit,"FF")==0)
                                             mbrr->mbr_partition_4.part_fit='F';
                                        else
                                             printf("!!HAY ERROR CON EL TYPE DE LA PARTICION\n");
                                        sprintf(nuevo->part_name,params->name);
                                        nuevo->part_next=ebractual.part_next;//EL SIGUIENTE DEL NUEVO
                                        nuevo->part_size= realsize;//EL TAM DEL NUEVO LOGICO
                                        nuevo->part_start=ebractual.part_start+ebractual.part_size+1+sizeof(EBR)+1;//LA POSICION DE DONDE EMPIEZA LA LOGICA
                                        //SE ESCRIBE EL NUEVO EBR
                                        fseek(fa,ebractual.part_start+ebractual.part_size+1,SEEK_SET);
                                        fwrite(nuevo,sizeof(EBR),1,fa);
                                        //SE MODIFICA EL EBR ACTUAL CON LA SIGUIENTE POS
                                        ebractual.part_next= ebractual.part_start+ebractual.part_size+1;
                                        //SE SOBREESCRIBE EL EBR ACTUAL
                                        fseek(fa,ebractual.part_start-sizeof(EBR)-1,SEEK_SET);
                                        fwrite(&ebractual,sizeof(EBR),1,fa);

                                        termino=1;
                                        free(nuevo);
                                        fclose(fa);





                                   }
                                   else
                                   {
                                        //SE SECE EL CONTROL AL SIGUIENTE
                                        //NO cabe, sedemos el control al siguiente EBR
                                        fseek(fa,ebractual.part_next,SEEK_SET);
                                        fread(&ebractual,sizeof(EBR),1,fa);
                                   }
                                   }

                              }


                    }//FIN WHILE !TERMINO


               }
               else
               {
                    printf("CLAVON EN GRABARPARTICIONL, NO ENCONTRE NINGUNA EXTENDIDA!\n");
               }





//---------------------------------------FIN LOGICAS------------------------------------------------------
               return 1;
          }else
          {
               return 1;
          }
     }
     else
          return 1;

}//FIN CREAR-PARTICION

void GrafMbr(Param*params,char*pathdisco)
{
     printf("\n EN GRAFICAR DISCO\n");
     char url3[512]="";
     strcpy(url3,params->path);
     crearDirectorio(url3);
     struct tm * timeinfo;


     FILE *fdot;
     FILE *fmbr;
     MBR mbrr;
//     EBR ebrr;
     fmbr = fopen(pathdisco, "rb");
     fdot = fopen("grafmbr.dot","w");          // Este modo permite leer y escribir
     if(fmbr)
     {
          fseek(fmbr,0,SEEK_SET);
          fread(&mbrr, sizeof(MBR), 1, fmbr);
          fprintf(fdot,"digraph mbr {\n");
          fprintf(fdot," rankdir=LR\n");
          fprintf(fdot," node [shape=plaintext]\n");
          fprintf(fdot," b [\n");
          fprintf(fdot,"      style=filled label=<\n");
          fprintf(fdot,"           <TABLE BORDER=\"1\" CELLBORDER=\"1\" CELLSPACING=\"2\">\n");
          fprintf(fdot,"                 <TR>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"2\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">MBR %s</FONT></TD>\n",pathdisco);
          fprintf(fdot,"                 </TR>\n");

          //-----encabezado
          fprintf(fdot,"                 <TR>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">Nombre</FONT></TD>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">Valor</FONT></TD>\n");
          fprintf(fdot,"                 </TR>\n");
          //---- mbr tam
          fprintf(fdot,"                 <TR>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">mbr_tamaño</FONT></TD>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_1.part_size);
          fprintf(fdot,"                 </TR>\n");
          //----fecha
          timeinfo = localtime ( &mbrr.mbr_fecha_creacion);
          char* fec= asctime(timeinfo);
          fprintf(fdot,"                 <TR>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">mbr_fecha_creacion</FONT></TD>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%s</FONT></TD>\n",fec);
          fprintf(fdot,"                 </TR>\n");
          //-----signatura
          fprintf(fdot,"                 <TR>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">mbr_disk_signature</FONT></TD>\n");
          fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_disk_signature);

          fprintf(fdot,"                 </TR>\n");





          if(mbrr.mbr_partition_1.part_status=='1')
          {
               //part status
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",1);

               fprintf(fdot,"                 </TR>\n");

               //part type

               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_type_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_1.part_type);

               fprintf(fdot,"                 </TR>\n");

               //---part fit
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_fit_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_1.part_fit);

               fprintf(fdot,"                 </TR>\n");


               //---part start
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_start_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_1.part_start);

               fprintf(fdot,"                 </TR>\n");

               //---part_size_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_size_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_1.part_size);

               fprintf(fdot,"                 </TR>\n");

               //---part_name_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_name_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%s</FONT></TD>\n",mbrr.mbr_partition_1.part_name    );

               fprintf(fdot,"                 </TR>\n");






          }
          else
          {
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_1</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",0);

               fprintf(fdot,"                 </TR>\n");
          }
          if(mbrr.mbr_partition_2.part_status=='1')
          {
               //part status
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",1);

               fprintf(fdot,"                 </TR>\n");

               //part type

               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_type_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_2.part_type);

               fprintf(fdot,"                 </TR>\n");

               //---part fit
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_fit_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_2.part_fit);

               fprintf(fdot,"                 </TR>\n");


               //---part start
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_start_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_2.part_start);

               fprintf(fdot,"                 </TR>\n");

               //---part_size_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_size_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_2.part_size);

               fprintf(fdot,"                 </TR>\n");

               //---part_name_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_name_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%s</FONT></TD>\n",mbrr.mbr_partition_2.part_name    );

               fprintf(fdot,"                 </TR>\n");


          }
          else
          {
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_2</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",0);

               fprintf(fdot,"                 </TR>\n");
          }
          if(mbrr.mbr_partition_3.part_status=='1')
          {


                     //part status
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",1);

               fprintf(fdot,"                 </TR>\n");

               //part type

               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_type_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_3.part_type);

               fprintf(fdot,"                 </TR>\n");

               //---part fit
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_fit_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_3.part_fit);

               fprintf(fdot,"                 </TR>\n");


               //---part start
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_start_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_3.part_start);

               fprintf(fdot,"                 </TR>\n");

               //---part_size_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_size_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_3.part_size);

               fprintf(fdot,"                 </TR>\n");

               //---part_name_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_name_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%s</FONT></TD>\n",mbrr.mbr_partition_3.part_name    );

               fprintf(fdot,"                 </TR>\n");



          }
          else
          {
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_3</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",0);

               fprintf(fdot,"                 </TR>\n");
          }
          if(mbrr.mbr_partition_4.part_status=='1')
          {


                //part status
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",1);

               fprintf(fdot,"                 </TR>\n");

               //part type

               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_type_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_4.part_type);

               fprintf(fdot,"                 </TR>\n");

               //---part fit
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_fit_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%c</FONT></TD>\n",mbrr.mbr_partition_4.part_fit);

               fprintf(fdot,"                 </TR>\n");


               //---part start
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_start_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_4.part_start);

               fprintf(fdot,"                 </TR>\n");

               //---part_size_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_size_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",mbrr.mbr_partition_4.part_size);

               fprintf(fdot,"                 </TR>\n");

               //---part_name_1
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_name_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%s</FONT></TD>\n",mbrr.mbr_partition_4.part_name    );

               fprintf(fdot,"                 </TR>\n");



          }
          else
          {
               fprintf(fdot,"                 <TR>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">part_status_4</FONT></TD>\n");
               fprintf(fdot,"                     <TD COLSPAN=\"1\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"16\">%d</FONT></TD>\n",0);

               fprintf(fdot,"                 </TR>\n");
          }
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");
          fprintf(fdot,"\n");

          fprintf(fdot,"           </TABLE>>  \n");
          fprintf(fdot,"   ]\n");
          fprintf(fdot,"}\n");
          fprintf(fdot,"\n");

          //-------------------------------EBR SSSSS-----------------------------
/*
                    PARTITION* particionl2
                    EBR ebractual;
                    fseek(fa,mbrr.mbr_partition_1.part_start,SEEK_SET);
                    fread(&ebractual,sizeof(EBR),1,fa);
                    if(ebractual.part_status=='1')
                    {
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       STATUS:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }
                    else
                         printf("       EL EBR DE DEFAULT ESTA VACIO\n");
                    while(ebractual.part_next!=-1)
                    {
                         fseek(fa,ebractual.part_next,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         printf("       PARTICION:%s\n",ebractual.part_name);
                         printf("       FIT:%c\n",ebractual.part_fit);
                         printf("       NEXT:%d\n",ebractual.part_next);
                         printf("       SIZE:%d\n",ebractual.part_size);
                         printf("       START:%d\n",ebractual.part_start);
                         printf("       NEXT:%c\n",ebractual.part_status);
                         printf("       -------------------------------\n");

                    }












*/


     }//fin if existe mbr
}//fin void GrafMbr

void GrafDisco(Param*params,char*pathdisco)
{
     printf("\n EN GRAFICAR DISCO\n");
     char url3[512]="";
     strcpy(url3,params->path);
     crearDirectorio(url3);


     FILE *fdot;
     FILE *fmbr;
     MBR mbrr;
     fmbr = fopen(pathdisco, "rb");
     fdot = fopen("grafd.dot","w");          // Este modo permite leer y escribir
     if(fmbr)
     {
          //existe,Esta bien!
          fseek(fmbr,0,SEEK_SET);
          fread(&mbrr, sizeof(MBR), 1, fmbr);


     fprintf(fdot,"digraph G {\n");
     fprintf(fdot," rankdir=LR\n");
     fprintf(fdot," node [shape=plaintext]\n");
     fprintf(fdot," b [\n");
     fprintf(fdot,"      style=filled label=<\n");
     fprintf(fdot,"           <TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"2\">\n");

     fprintf(fdot,"            <TR>\n");
     //Se grafica MBR
     fprintf(fdot,"            <TD COLSPAN=\"20\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">MBR</FONT></TD>\n");
     //-----------------------------------------------------------

     if(mbrr.mbr_partition_1.part_status=='1')
     {
          int backpart1=mbrr.mbr_partition_1.part_start-(sizeof(MBR)+1)-1;//aqui verificamos si no hay espacio entre el mbr y la particion1

          if(backpart1>0)
          {
               // si hay espacio entre la P1 y el mbr
               int colsp0 = backpart1/1024;
               if(colsp0==0)
                    colsp0=1;


               int colspanp0 = (colsp0*300)/1024;

               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanp0);
          }

          if(mbrr.mbr_partition_1.part_type=='P')
          {
               int colspanp1 = (mbrr.mbr_partition_1.part_size*300)/(1024*1024);
               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">%s</FONT></TD>\n",colspanp1,mbrr.mbr_partition_1.part_name);
          }
          else
          {
               //aqui hay que graficar la extendida
               int colspanp1 = (mbrr.mbr_partition_1.part_size*300)/(1024*1024);
               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\">\n",colspanp1);
               fprintf(fdot,"                <TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"2\">\n");
               fprintf(fdot,"                     <TR>\n");
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"12.0\">EXTENDIDA-%s</FONT></TD>\n",colspanp1,mbrr.mbr_partition_1.part_name);
               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                     <TR>\n");

               //-------
               EBR ebractual;
               fseek(fmbr,mbrr.mbr_partition_1.part_start,SEEK_SET);
               fread(&ebractual, sizeof(EBR), 1, fmbr);

               fprintf(fdot,"                         <TD COLSPAN=\"10\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
               if(ebractual.part_status=='1')
               {
                    int colspp = (ebractual.part_size*300)/1048576;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);

               }


               while(ebractual.part_next!=-1)
               {
                    //MIENTRAS QUE EL ACTUAL tenga siguiente
                    //VER SI HAY ESPACIO ENTRE EL SIGUIENTE ANTES DE CAMBIAR AL SIGUIENTE.
                    int between = ebractual.part_next-(ebractual.part_start+ebractual.part_size)-1;
                    if(between>0)
                    {
                         between = between/1024;
                         if(between==0)
                              between=1;

                         int colsppp = (between*300)/1024;
                         fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


                    }
                    //SE CAMBIA EL ACTUAL A SU SIGUIENTE Y SE GRAFICA
                    fseek(fmbr,ebractual.part_next,SEEK_SET);
                    fread(&ebractual, sizeof(EBR), 1, fmbr);
                    fprintf(fdot,"                         <TD COLSPAN=\"15\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
                    int colspp = (ebractual.part_size*300)/1048576;
                    fprintf(fdot,"                           <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);


               }
               //SE VA A GRAFICAR EL POSIBLE ESPACIO QUE QUEDO DE ULTIMO EN EL EXTENDIDO
               int spacefinal = (mbrr.mbr_partition_1.part_size) - (ebractual.part_size);
               printf("LO Q TIENE SPACE FINAL ANTES DE LA OPERACION:%d\n",spacefinal);
               if(spacefinal>0)
               {
                    spacefinal = spacefinal/1024;

                    if(spacefinal==0)
                         spacefinal=1;

                         int colsppp = (spacefinal*300)/1024;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


               }


               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                </TABLE>\n");
               fprintf(fdot,"            </TD>\n");


          }
          if(mbrr.mbr_partition_2.part_status=='1')
          {
               //Se grafica el posible espacio entre P1 y P2
                int spacebetween1y2 = mbrr.mbr_partition_2.part_start - mbrr.mbr_partition_1.part_size - mbrr.mbr_partition_1.part_start-1;
                if(spacebetween1y2>0)
                {
                    spacebetween1y2 = spacebetween1y2/1024;
                    if(spacebetween1y2==0)
                         spacebetween1y2=1;

                    int colspanp1p2 = (spacebetween1y2*300)/1024;
                    fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanp1p2);
                }

                //Se grafica la particion2
               if(mbrr.mbr_partition_2.part_type=='P')
               {
                    int colspanp2 = (mbrr.mbr_partition_2.part_size*300)/1048576;
                    fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">%s</FONT></TD>\n",colspanp2,mbrr.mbr_partition_2.part_name);
               }
               else
               {
                   //aqui hay que graficar la extendida
               int colspanp1 = (mbrr.mbr_partition_2.part_size*300)/(1024*1024);
               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\">\n",colspanp1);
               fprintf(fdot,"                <TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"2\">\n");
               fprintf(fdot,"                     <TR>\n");
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"12.0\">EXTENDIDA-%s</FONT></TD>\n",colspanp1,mbrr.mbr_partition_2.part_name);
               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                     <TR>\n");

               //-------
               EBR ebractual;
               fseek(fmbr,mbrr.mbr_partition_2.part_start,SEEK_SET);
               fread(&ebractual, sizeof(EBR), 1, fmbr);

               fprintf(fdot,"                         <TD COLSPAN=\"10\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
               if(ebractual.part_status=='1')
               {
                    int colspp = (ebractual.part_size*300)/1048576;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);

               }


               while(ebractual.part_next!=-1)
               {
                    //MIENTRAS QUE EL ACTUAL tenga siguiente
                    //VER SI HAY ESPACIO ENTRE EL SIGUIENTE ANTES DE CAMBIAR AL SIGUIENTE.
                    int between = ebractual.part_next-(ebractual.part_start+ebractual.part_size)-1;
                    if(between>0)
                    {
                         between = between/1024;
                         if(between==0)
                              between=1;

                         int colsppp = (between*300)/1024;
                         fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


                    }
                    //SE CAMBIA EL ACTUAL A SU SIGUIENTE Y SE GRAFICA
                    fseek(fmbr,ebractual.part_next,SEEK_SET);
                    fread(&ebractual, sizeof(EBR), 1, fmbr);
                    fprintf(fdot,"                         <TD COLSPAN=\"15\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
                    int colspp = (ebractual.part_size*300)/1048576;
                    fprintf(fdot,"                           <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);


               }
               //SE VA A GRAFICAR EL POSIBLE ESPACIO QUE QUEDO DE ULTIMO EN EL EXTENDIDO
               int spacefinal = (mbrr.mbr_partition_2.part_size) - (ebractual.part_size);
               printf("LO Q TIENE SPACE FINAL ANTES DE LA OPERACION:%d\n",spacefinal);
               if(spacefinal>0)
               {
                    spacefinal = spacefinal/1024;

                    if(spacefinal==0)
                         spacefinal=1;

                         int colsppp = (spacefinal*300)/1024;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


               }


               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                </TABLE>\n");
               fprintf(fdot,"            </TD>\n");
               }

               if(mbrr.mbr_partition_3.part_status=='1')
               {
                    //Se grafica el posible espacio entre P2 y P3
                    int spacebetween2y3 = mbrr.mbr_partition_3.part_start - mbrr.mbr_partition_2.part_size - mbrr.mbr_partition_2.part_start-1;
                    if(spacebetween2y3>0)
                    {
                         spacebetween2y3 = spacebetween2y3/1024;
                         if(spacebetween2y3==0)
                              spacebetween2y3=1;
                         int colspanp2p3 = (spacebetween2y3*300)/1024;
                         fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanp2p3);
                    }

                    //Se grafica la particion3
                    if(mbrr.mbr_partition_3.part_type=='P')
                    {
                         int colspanp3 = (mbrr.mbr_partition_3.part_size*300)/1048576;
                         fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">%s</FONT></TD>\n",colspanp3,mbrr.mbr_partition_3.part_name);
                    }
                    else
                    {
                         //aqui hay que graficar la extendida
               int colspanp1 = (mbrr.mbr_partition_3.part_size*300)/(1024*1024);
               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\">\n",colspanp1);
               fprintf(fdot,"                <TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"2\">\n");
               fprintf(fdot,"                     <TR>\n");
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"12.0\">EXTENDIDA-%s</FONT></TD>\n",colspanp1,mbrr.mbr_partition_3.part_name);
               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                     <TR>\n");

               //-------
               EBR ebractual;
               fseek(fmbr,mbrr.mbr_partition_3.part_start,SEEK_SET);
               fread(&ebractual, sizeof(EBR), 1, fmbr);

               fprintf(fdot,"                         <TD COLSPAN=\"10\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
               if(ebractual.part_status=='1')
               {
                    int colspp = (ebractual.part_size*300)/1048576;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);

               }


               while(ebractual.part_next!=-1)
               {
                    //MIENTRAS QUE EL ACTUAL tenga siguiente
                    //VER SI HAY ESPACIO ENTRE EL SIGUIENTE ANTES DE CAMBIAR AL SIGUIENTE.
                    int between = ebractual.part_next-(ebractual.part_start+ebractual.part_size)-1;
                    if(between>0)
                    {
                         between = between/1024;
                         if(between==0)
                              between=1;

                         int colsppp = (between*300)/1024;
                         fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


                    }
                    //SE CAMBIA EL ACTUAL A SU SIGUIENTE Y SE GRAFICA
                    fseek(fmbr,ebractual.part_next,SEEK_SET);
                    fread(&ebractual, sizeof(EBR), 1, fmbr);
                    fprintf(fdot,"                         <TD COLSPAN=\"15\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
                    int colspp = (ebractual.part_size*300)/1048576;
                    fprintf(fdot,"                           <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);


               }
               //SE VA A GRAFICAR EL POSIBLE ESPACIO QUE QUEDO DE ULTIMO EN EL EXTENDIDO
               int spacefinal = (mbrr.mbr_partition_3.part_size) - (ebractual.part_size);
               printf("LO Q TIENE SPACE FINAL ANTES DE LA OPERACION:%d\n",spacefinal);
               if(spacefinal>0)
               {
                    spacefinal = spacefinal/1024;

                    if(spacefinal==0)
                         spacefinal=1;

                         int colsppp = (spacefinal*300)/1024;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


               }


               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                </TABLE>\n");
               fprintf(fdot,"            </TD>\n");
                    }

                    if(mbrr.mbr_partition_4.part_status=='1')
                    {
                         //Se grafica el posible espacio entre P3 y P4
                         int spacebetween3y4 = mbrr.mbr_partition_4.part_start - mbrr.mbr_partition_3.part_size - mbrr.mbr_partition_3.part_start-1;
                         if(spacebetween3y4>0)
                         {
                              spacebetween3y4 = spacebetween3y4/1024;
                              if(spacebetween3y4==0)
                                   spacebetween3y4=1;
                              int colspanp3p4 = (spacebetween3y4*300)/1048576;
                              fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanp3p4);
                         }

                         //Se grafica la particion4
                         if(mbrr.mbr_partition_4.part_type=='P')
                         {
                              int colspanp4 = (mbrr.mbr_partition_4.part_size*300)/1048576;
                              fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">%s</FONT></TD>\n",colspanp4,mbrr.mbr_partition_4.part_name);
                         }
                         else
                         {
                              //aqui hay que graficar la extendida
               int colspanp1 = (mbrr.mbr_partition_4.part_size*300)/(1024*1024);
               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\">\n",colspanp1);
               fprintf(fdot,"                <TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"2\">\n");
               fprintf(fdot,"                     <TR>\n");
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"1\"><FONT POINT-SIZE=\"12.0\">EXTENDIDA-%s</FONT></TD>\n",colspanp1,mbrr.mbr_partition_4.part_name);
               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                     <TR>\n");

               //-------
               EBR ebractual;
               fseek(fmbr,mbrr.mbr_partition_4.part_start,SEEK_SET);
               fread(&ebractual, sizeof(EBR), 1, fmbr);

               fprintf(fdot,"                         <TD COLSPAN=\"10\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
               if(ebractual.part_status=='1')
               {
                    int colspp = (ebractual.part_size*300)/1048576;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);

               }


               while(ebractual.part_next!=-1)
               {
                    //MIENTRAS QUE EL ACTUAL tenga siguiente
                    //VER SI HAY ESPACIO ENTRE EL SIGUIENTE ANTES DE CAMBIAR AL SIGUIENTE.
                    int between = ebractual.part_next-(ebractual.part_start+ebractual.part_size)-1;
                    if(between>0)
                    {
                         between = between/1024;
                         if(between==0)
                              between=1;

                         int colsppp = (between*300)/1024;
                         fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


                    }
                    //SE CAMBIA EL ACTUAL A SU SIGUIENTE Y SE GRAFICA
                    fseek(fmbr,ebractual.part_next,SEEK_SET);
                    fread(&ebractual, sizeof(EBR), 1, fmbr);
                    fprintf(fdot,"                         <TD COLSPAN=\"15\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">EBR</FONT></TD>\n");
                    int colspp = (ebractual.part_size*300)/1048576;
                    fprintf(fdot,"                           <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">LOGICA</FONT></TD>",colspp);


               }
               //SE VA A GRAFICAR EL POSIBLE ESPACIO QUE QUEDO DE ULTIMO EN EL EXTENDIDO
               int spacefinal = (mbrr.mbr_partition_4.part_size) - (ebractual.part_size);
               printf("LO Q TIENE SPACE FINAL ANTES DE LA OPERACION:%d\n",spacefinal);
               if(spacefinal>0)
               {
                    spacefinal = spacefinal/1024;

                    if(spacefinal==0)
                         spacefinal=1;

                         int colsppp = (spacefinal*300)/1024;
               fprintf(fdot,"                          <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colsppp);


               }


               fprintf(fdot,"                     </TR>\n");
               fprintf(fdot,"                </TABLE>\n");
               fprintf(fdot,"            </TD>\n");
                         }
                    }
                    else
                    {
                         //No esta ocupado nada, todo lo demas es libre!
                         int librep4 = mbrr.mbr_tamano- (mbrr.mbr_partition_3.part_start + mbrr.mbr_partition_3.part_size);
                         if(librep4>0)
                         {
                              librep4 = librep4*300/1024;
                              if(librep4==0)
                                   librep4=1;
                              int colspanlibrep4 = (librep4*300)/1024;
                              fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanlibrep4);
                         }
                    }


               }
               else
               {

                     //No esta ocupado nada, todo lo demas es libre!
                    int librep3 = mbrr.mbr_tamano- (mbrr.mbr_partition_2.part_start + mbrr.mbr_partition_2.part_size);
                    if(librep3>0)
                    {
                         librep3 = librep3/1024;
                         if(librep3==0)
                              librep3=1;
                         int colspanlibrep3 = (librep3*300)/1024;
                         fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanlibrep3);
                    }
               }




          }

          else
          {
               //No esta ocupado nada, todo lo demas es libre!

               int librep2 = mbrr.mbr_tamano- (mbrr.mbr_partition_1.part_start + mbrr.mbr_partition_1.part_size);
               if(librep2>0)
               {
                    librep2 = librep2/1024;
                    if(librep2==0)
                    librep2=1;
                    int colspanlibrep2 = (librep2*300)/1024;
                    fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanlibrep2);
               }


          }


     }//hasta aqui bien
     else
     {

          int librep1 = mbrr.mbr_tamano-137;
          if(librep1>0)
          {
               librep1 = librep1/1024;
               if(librep1==0)
                    librep1=1;
               int colspanlibrep1 = (librep1*300)/1024;
               fprintf(fdot,"            <TD COLSPAN=\"%d\" ROWSPAN=\"3\"><FONT POINT-SIZE=\"12.0\">FREE</FONT></TD>\n",colspanlibrep1);
               }

     }


     //-----------------------------------------------------------
     fprintf(fdot,"            </TR>");
     fprintf(fdot,"           </TABLE>>  \n");
     fprintf(fdot,"   ]\n");
     fprintf(fdot,"}\n");
     fprintf(fdot,"\n");
     fflush(fdot);
     fclose(fdot);
     fclose(fmbr);
}
else
     printf("ERROR EN GRAFICAR!, NO EXISTE TAL DISCO\n");
}



void crearDirectorio(char path[512])
{
    char *pch3;
    char pch4[512]="";
    char *pch5;
    pch3 = strtok (path,"/");
    pch5 = strtok (NULL,"/");
    while(pch5 !=NULL)
    {
    strcat(pch4,"/");
    strcat(pch4,pch3);
    strcpy(pch3,pch5);
    pch5 = strtok (NULL,"/");
    mkdir(pch4, S_IRWXU | S_IRWXG | S_IRWXO|  S_IROTH | S_IXOTH);
    }

}

void crearArchivo(char path[512])
{
    FILE *ptr3;
    ptr3 = fopen(path,"wa");
    fclose(ptr3);
}

//----------------------MOUNTURAS
DMOUNT* crearDmount(Param* params,char letter)
{


          DMOUNT* disco;
          disco = (DMOUNT*)calloc(1,sizeof(DMOUNT));
          sprintf(disco->path,params->path);
          disco->letter = letter;
          disco->lnmount = (LNMOUNT*)calloc(1,sizeof(LNMOUNT));
          disco->lnmount->primero=0;
          disco->lnmount->ultimo=0;
          disco->lnmount->cont=0;
          disco->siguiente=0;
          disco->anterior=0;
          return disco;

}

int LMVacia(LMOUNT* lista)
{
     return lista->primero==0;
}

DMOUNT* BuscarDmount(LMOUNT *lista,Param *params)
{

     printf("EN BUSCAR DMOUNT\n");
      if(!LMVacia(lista))
      {
          DMOUNT *aux = lista->primero;
          while(aux)
          {
               printf("ENTRO!!!!!%s!!!!!\n",aux->path);
               if(strcmp(aux->path,params->path)==0)
                    return aux;
               else
                    aux = aux->siguiente;

          }
          return aux;
      }
      return 0;

}

int BuscarEnLNM(LNMOUNT*lista,char* nombre)
{


      NMOUNT* aux = lista->primero;
     while(aux)
     {
          if(strcmp(nombre,aux->name)==0)
               return 1;
          else
               aux = aux->siguiente;
     }
     return 0;
}
void AddInLNM(LNMOUNT *lista,Param* params,char letter)
{
     if(!lista->primero)
     {
          //SI ESTA VACIA LA LNMOUNTlist
          lista->primero = (NMOUNT*)calloc(1,sizeof(NMOUNT));
          sprintf(lista->primero->name,params->name);
          lista->primero->id[0]='v';
          lista->primero->id[1]='d';
          lista->primero->id[2]=letter;
          lista->primero->id[3]= (char) (lista->cont + 48);
          lista->primero->anterior=0;
          lista->primero->siguiente=0;
          lista->ultimo = lista->primero;
          lista->cont++;

     }
     else
     {
          //NO ESTA VACIA LA LNMOUNT
          if(!BuscarEnLNM(lista,params->name))
          {
               NMOUNT* aux = lista->primero;
               if(aux->id[3]!='a')
               {
                         //SI EL PRIMERO NO ES PRIMERO EN REALIDAD.
                         lista->primero->anterior = (NMOUNT*)calloc(1,sizeof(NMOUNT));
                         sprintf(lista->primero->anterior->name,params->name);
                         lista->primero->anterior->id[0]='v';
                         lista->primero->anterior->id[1]='d';
                         lista->primero->anterior->id[2]=letter;
                         lista->primero->anterior->id[3]= 'a';
               }
               else
               {
                    int flagg=1;
                    while(aux->siguiente)
                    {
                         if(aux->siguiente->id[3]>aux->id[3]+1)
                         {
                              //si su siguiente tiene de diferencia mas de uno
                              //es por que se borro uno o mas entre ellos.
                              NMOUNT*nuevo = (NMOUNT*)calloc(1,sizeof(NMOUNT));
                              sprintf(nuevo->name,params->name);
                              nuevo->id[0]='v';
                              nuevo->id[1]='d';
                              nuevo->id[2]=letter;
                              nuevo->id[3]= aux->id[3] + 1;
                              nuevo->anterior=aux;
                              nuevo->siguiente = aux->siguiente;
                              nuevo->anterior->siguiente=nuevo;
                              nuevo->siguiente->anterior=nuevo;
                              flagg=0;
                         }
                         aux = aux->siguiente;
                    }
                    if(flagg)
                    {
                         //SE METE YA QUE NO ENCONTRO ESPACIOS DE POR MEDIO
                         lista->ultimo->siguiente = (NMOUNT*)calloc(1,sizeof(NMOUNT));
                         sprintf(lista->ultimo->siguiente->name,params->name);
                         lista->ultimo->siguiente->id[0]='v';
                         lista->ultimo->siguiente->id[1]='d';
                         lista->ultimo->siguiente->id[2]=letter;
                         lista->ultimo->siguiente->id[3]= (char) (lista->cont + 48);
                         lista->ultimo->siguiente->anterior=lista->ultimo;
                         lista->ultimo->siguiente->siguiente=0;
                         lista->ultimo = lista->ultimo->siguiente;
                         lista->cont++;
                    }
               }




          }
          else
               printf("LA PARTICION YA ESTA MONTADA!");
     }


}//FIN AddInLNM

int ElimInLNM(LNMOUNT *lista,Param* params)
{
     NMOUNT* aux = lista->primero;
     while(aux)
     {
          if(strcmp(aux->id,params->id)==0)
          {
               //SI ENCONTRO EL ID A DESMONTAR.
               if(aux==lista->primero)
               {
                    lista->primero = aux->siguiente;
                    aux->siguiente->anterior = 0;
                    free(aux);
                    return 1;
               }
               else if(aux==lista->ultimo)
               {
                    lista->ultimo = aux->anterior;
                    aux->anterior->siguiente=0;
                    free(aux);
                    return 1;
               }
               else
               {
                    aux->anterior->siguiente = aux->siguiente;
                    aux->siguiente->anterior = aux->anterior;
                    free(aux);
                    return 1;
               }
          }
          aux = aux->siguiente;
     }
     return 0;
}//FIN  ElinInLNM


void AddInLM(LMOUNT *lista,Param* params)
{
     if(!lista)
          printf("LA LISTALM ES NULA\n");

     MBR mbrr;
     PARTITION *particion = 0;
     //EBR *particionlogica=0;
     int eslogica=0;
     int encontrada=0;
     FILE *fa;
     fa = fopen(params->path, "rb");
     if(fa)
     {
          //SI EXISTE EL ARCHIVO DEL DISCO
          fseek(fa,0,SEEK_SET);
          fread(&mbrr, sizeof(MBR), 1, fa);

          if(mbrr.mbr_partition_1.part_status=='1'&&!encontrada)
          {
               if(mbrr.mbr_partition_1.part_type=='E')
               {
                    if(strcmp(mbrr.mbr_partition_1.part_name,params->name)==0)
                         printf("ERROR, SE QUIERE MONTAR UNA EXTENDIDA");
                    else
                    {
                         //SE BUSCA DENTRO DE LA EXTENDIDA
                         EBR ebractual;
                         fseek(fa,mbrr.mbr_partition_1.part_start,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         if(ebractual.part_status=='1')
                         {
                              if(strcmp(ebractual.part_name,params->name)==0)
                              {
                                   //EL PRIMER EBR ES EL BUSCADO
                                   //particionlogica=&ebractual;
                                   eslogica=1;
                                   encontrada=1;
                              }
                         }
                         if(!encontrada)
                         {
                              while(ebractual.part_next!=-1)
                              {
                                   //SE CARGA AL ACTUAL EL SIGUIENTE EBR
                                   fseek(fa,ebractual.part_next,SEEK_SET);
                                   fread(&ebractual,sizeof(EBR),1,fa);
                                   if(strcmp(ebractual.part_name,params->name)==0)
                                   {
                                        //EL PRIMER EBR ES EL BUSCADO
                                        //particionlogica=&ebractual;
                                        eslogica=1;
                                        encontrada=1;
                                   }
                              }
                         }
                    }


               }
               else if(strcmp(mbrr.mbr_partition_1.part_name,params->name)==0)
                    particion = &mbrr.mbr_partition_1;

          }
          if(mbrr.mbr_partition_2.part_status=='1'&&!encontrada)
          {

             if(mbrr.mbr_partition_2.part_type=='E')
               {
                    if(strcmp(mbrr.mbr_partition_2.part_name,params->name)==0)
                         printf("ERROR, SE QUIERE MONTAR UNA EXTENDIDA");
                    else
                    {
                         //SE BUSCA DENTRO DE LA EXTENDIDA
                         EBR ebractual;
                         fseek(fa,mbrr.mbr_partition_2.part_start,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         if(ebractual.part_status=='1')
                         {
                              if(strcmp(ebractual.part_name,params->name)==0)
                              {
                                   //EL PRIMER EBR ES EL BUSCADO
                                  // particionlogica=&ebractual;
                                   eslogica=1;
                                   encontrada=1;
                              }
                         }
                         if(!encontrada)
                         {
                              while(ebractual.part_next!=-1)
                              {
                                   //SE CARGA AL ACTUAL EL SIGUIENTE EBR
                                   fseek(fa,ebractual.part_next,SEEK_SET);
                                   fread(&ebractual,sizeof(EBR),1,fa);
                                   if(strcmp(ebractual.part_name,params->name)==0)
                                   {
                                        //EL PRIMER EBR ES EL BUSCADO
                                        //particionlogica=&ebractual;
                                        eslogica=1;
                                        encontrada=1;
                                   }
                              }
                         }
                    }


               }
               else if(strcmp(mbrr.mbr_partition_2.part_name,params->name)==0)
                    particion = &mbrr.mbr_partition_2;








          }
          if(mbrr.mbr_partition_3.part_status=='1'&&!encontrada)
          {

               if(mbrr.mbr_partition_3.part_type=='E')
               {
                    if(strcmp(mbrr.mbr_partition_3.part_name,params->name)==0)
                         printf("ERROR, SE QUIERE MONTAR UNA EXTENDIDA");
                    else
                    {
                         //SE BUSCA DENTRO DE LA EXTENDIDA
                         EBR ebractual;
                         fseek(fa,mbrr.mbr_partition_3.part_start,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         if(ebractual.part_status=='1')
                         {
                              if(strcmp(ebractual.part_name,params->name)==0)
                              {
                                   //EL PRIMER EBR ES EL BUSCADO
                                   //particionlogica=&ebractual;
                                   eslogica=1;
                                   encontrada=1;
                              }
                         }
                         if(!encontrada)
                         {
                              while(ebractual.part_next!=-1)
                              {
                                   //SE CARGA AL ACTUAL EL SIGUIENTE EBR
                                   fseek(fa,ebractual.part_next,SEEK_SET);
                                   fread(&ebractual,sizeof(EBR),1,fa);
                                   if(strcmp(ebractual.part_name,params->name)==0)
                                   {
                                        //EL PRIMER EBR ES EL BUSCADO
                                        //particionlogica=&ebractual;
                                        eslogica=1;
                                        encontrada=1;
                                   }
                              }
                         }
                    }


               }
               else if(strcmp(mbrr.mbr_partition_3.part_name,params->name)==0)
                    particion = &mbrr.mbr_partition_3;








          }
          if(mbrr.mbr_partition_4.part_status=='1'&&!encontrada)
          {

               if(mbrr.mbr_partition_4.part_type=='E')
               {
                    if(strcmp(mbrr.mbr_partition_4.part_name,params->name)==0)
                         printf("ERROR, SE QUIERE MONTAR UNA EXTENDIDA");
                    else
                    {
                         //SE BUSCA DENTRO DE LA EXTENDIDA
                         EBR ebractual;
                         fseek(fa,mbrr.mbr_partition_4.part_start,SEEK_SET);
                         fread(&ebractual,sizeof(EBR),1,fa);
                         if(ebractual.part_status=='1')
                         {
                              if(strcmp(ebractual.part_name,params->name)==0)
                              {
                                   //EL PRIMER EBR ES EL BUSCADO
                                   //particionlogica=&ebractual;
                                   eslogica=1;
                                   encontrada=1;
                              }
                         }
                         if(!encontrada)
                         {
                              while(ebractual.part_next!=-1)
                              {
                                   //SE CARGA AL ACTUAL EL SIGUIENTE EBR
                                   fseek(fa,ebractual.part_next,SEEK_SET);
                                   fread(&ebractual,sizeof(EBR),1,fa);
                                   if(strcmp(ebractual.part_name,params->name)==0)
                                   {
                                        //EL PRIMER EBR ES EL BUSCADO
                                        //particionlogica=&ebractual;
                                        eslogica=1;
                                        encontrada=1;
                                   }
                              }
                         }
                    }


               }
               else if(strcmp(mbrr.mbr_partition_4.part_name,params->name)==0)
                    particion = &mbrr.mbr_partition_4;






          }



          if(particion==0 &&!eslogica)
          {
               printf("ERROR!, NO EXISTE LA PARTICION QUE SE QUIERE MONTAR\n");
          }
          else
          {

               if(LMVacia(lista))
               {


                    //AQUI SE VERIFICA SI LA PARTICION EN VERDAD EXISTE EN EL MBR

                    //Si existe la pariticion, COMO ES EL PRIMER DISCO QUE SE CREA NO SE
                    //REVISA QUE NO ESTE MONTADA.
                    lista->primero = crearDmount(params,lista->lact);
                    lista->primero->letter=lista->lact;
                    AddInLNM(lista->primero->lnmount,params,lista->lact);
                    sprintf(lista->primero->path,params->path);
                    lista->ultimo = lista->primero;
                    lista->lact++;



               }
               else
               {
                    // LA LISTA NO ESTA VACIA
                    DMOUNT *disco = BuscarDmount(lista,params);
                    if(!disco)
                    {
                         disco = crearDmount(params,lista->lact);
                         disco->letter = lista->lact;
                         AddInLNM(lista->primero->lnmount,params,lista->lact);
                         sprintf(disco->path,params->path);
                         disco->anterior = lista->ultimo;
                         lista->ultimo->siguiente = disco;
                         lista->ultimo = disco;
                         lista->lact++;
                    }
                    else
                    {
                         AddInLNM(lista->primero->lnmount,params,lista->lact);

                    }

               }
          }

     }
     else
     {
          printf("NO EXISTE EL DISCO!\n");
     }





}
//FIN ADD
DMOUNT* BuscarEnLM(LMOUNT *lista,Param* params)
{
     printf("EN BuscarEnLM\n");
     DMOUNT *aux = lista->primero;
     while(aux)
     {
          if(aux->letter==params->id[2])
          {
               printf("Entro a q tienen la misma letra:%c\n",aux->letter);
               int resp= BuscarEnLNM(aux->lnmount,params->id);
               if(resp)
                    return aux;
               else
                    return 0;

          }
          else
               aux = aux->siguiente;
     }
     return aux;
}


void leerLM(LMOUNT *lista)
{
     printf("EN LEER-LM\n");
     if(!LMVacia(lista))
     {
          DMOUNT* aux = lista->primero;
          while(aux)
          {
               printf("DISCO MONTADO:%c\n",aux->letter);
               printf("PATH:%s\n",aux->path);
               printf("PARTICIONES MONTADAS:\n");
               NMOUNT* parti = aux->lnmount->primero;
               while(parti)
               {
                    printf("    NOMBRE:%s  ... ID:%s\n",parti->name,parti->id);
                    parti=parti->siguiente;
               }

               aux = aux->siguiente;
          }
     }
     else
     {
          printf("LA LISTA ESTA VACIA! ");
     }
}
